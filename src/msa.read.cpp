/*
 * msa.read.cpp
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
#include "msa/msa.h"

int main(int argc, char **argv)
{
  msa::MSA msa;
  msa.reader(std::cin);
  msa::Columns cols = msa.columns();
  cols.print();

  msa::Rows rows = msa.rows();
  rows.print();
  return 0;
}
