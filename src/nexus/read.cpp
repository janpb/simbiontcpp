/*
* nexus/read.cpp
* Copyright 2014 Jan <lejosh@members.fsf.org>
*
* Description:
* The implementations for parsing NEXUS files [0, 1].
*
* [0] http://hydrodictyon.eeb.uconn.edu/eebedia/index.php/
*     Phylogenetics:_NEXUS_Format
* [1] Maddison, David R., Swofford, David L. and Maddison, Wayne P. 1997.
*     NEXUS: an extensible file format for systematic information.
*     Systematic Biology 46: 590-621
*
* Comments:
*
*/

#include "nexus/read.h"

namespace nexus
{

  void Reader::read(std::istream& src)
  {
    std::ios_base::sync_with_stdio(false);
    std::vector<char> fil;
    char buffer[4 * 1024];
    std::cerr << "start\n";
    while(true)
    {
      src.read(buffer, sizeof(buffer));
      unsigned len = src.gcount();
      for(auto i = 0u; i < len; ++i)
      {
        fil.push_back(buffer[i]);
      }
      if(len < sizeof(buffer)){break;}
    }
    size = fil.size();
    std::cerr << "finish\n";
    pool = new char[size+1];
    for(auto i = 0u; i < size; ++i)
    {
      pool[i] = fil[i];
    }
    pool[size] = '\0';
  }

  void Reader::scan()
  {
    char* src = pool;
    char* sink = src;
    char* block = nullptr;
    unsigned stat = 0;
    while(true)
    {
      //std::cout << "stat: " << stat << " => "<< *src << "\n";
      switch(stat)
      {
        case 0:
          if(*src == '\n')
          {
            *src++ = '\0';  //  Check for #[NEXUS|nexus] ?
            stat = 1;
            break;
          }
        ++src;
        break;

        case 1:
          if(tolower(*src) == 'b')
          {
            sink = src++;
            stat = 2;
            break;
          }
          ++src;
        break;

        case 2:
          if(*src == ' ')
          {
            *src++ = '\0';  // Do some checking if we really have a 'Begin'
            block = src;
            break;
          }
          if(*src == ';')
          {
            *src++ = '\0';
            break;
          }
          if(*src == '\n')
          {
            ++src;
            //Reader::Parser parse;
            if(std::string(block) == "DATA") // id block, set parser
            {

              std::cerr << "Parse "<< block << "\n";
              src = parse_data(src);
              stat = 1;
              break;
            }
            stat = 1;
            break;
          }
          ++src;
        break;

        case 3:                       // collect block.
          if(*src == '\n')
          {
            *sink++ = *src++;
            stat = 4;
            break;
          }
          *sink++ = *src++;
        break;

        case 4:
          if(tolower(*src) == 'e')  // putative end of block
          {
            sink = src;
            src += 3;
            //std::cout << *src << "\n";
            stat = 5;
            break;
          }
          stat = 3;
        break;

        case 5:
          if(*src == ';')
          {
            *src = '\0';
            if(std::string(sink) == "End")  //not very elegant
            {
              //std::cout << "BEG BLOCK\n"
                        //<<    block
                        //<< "\nEND BLOCK\n";
            }
            stat = 3;
            break;
          }
          stat = 3;
        break;
      }
      if(*src == '\0')
      {
        std::cerr << "EOF\n";
        return;
      }
    }
  }

  char* Reader::parse_data(char* src)
  {
    char* sink = src;
    char* cmd  = nullptr;
    char* arg  = nullptr;
    char* id   = nullptr;
    char* seq  = nullptr;
    unsigned stat = 0;
    unsigned loc_stat = 0;
    while(true)
    {
      //std::cout << "d_stat " << stat << "=>" << *src << "  sav_stat:" << sav_stat <<"\n";
      switch(stat)
      {
        case 0:
          if(int(*src) >= 65) // skip \s and \t
          {
            cmd  = src;
            sink = src;
            stat = 1;
            break;
          }
          *sink++ = *src++;
        break;

        case 1:
          if(*src == ' ' || *src == '\n')
          {
            *src = '\0';
            //std::cout << "CMD: " << cmd << "\n";
            arg  = src++;
            if(std::string(cmd) == "Matrix")
            {
              stat = 20;
              break;
            }
            stat = 2;
            break;
          }
          if(*src == ';')
          {
            *src++ = '\0';
            //std::cout << cmd << "End?\n";
            return src;
          }
          *sink++ = *src++;
        break;

        case 2:
          if(*src == '[')
          {
            ++src;
            loc_stat = stat;
            stat = 10;
            break;
          }
          if(*src == ';')
          {
            *src  = '\0';
            *sink = '\0';
            //std::cout << "ARGS: " << arg << "\n";
            stat = 0;
            break;
          }
          *sink++ = *src++;

        break;

        /* 10 -> 19: status for comments and such */
        case 10:
          if(*src == ']')
          {
            ++src;
            stat = loc_stat;
            break;
          }
          ++src;
        break;

        /* 20 -> 29: status for matrix command in DATA block */
        case 20:
          if(*src == '[')
          {
            ++src;
            loc_stat = stat;
            stat = 10;
            break;
          }
          if(*src == ';')
          {
            *src  = '\0';
            *sink = '\0';
            //std::cout << "ARGSSSSS: " << arg << "\n";
            stat = 0;
            break;
          }
          //if(*src != ' ') // segmentation fault?
          if(int(*src) >= 33) // skip \s and \t
          {
            id = src;
            stat = 21;
            break;
          }
          ++src;
        break;

        case 21:
          if(*src == ' ')
          {
            *src++ = '\0';
            stat = 22;
            break;
          }
          *sink++ = *src++;
        break;

        case 22:
          if(*src != ' ')
          {
            seq = src;
            stat = 23;
            break;
          }
          ++src;
        break;

        case 23:
          if(*src == '\n')
          {
            *src++ ='\0';
            std::cout << ">" << id << "\n" << seq << "  \n";
            //data.seqs[id] = seq;
            //data.mtx[0]
            data.mtx[seq_nr][id] = seq;
            ++seq_nr;
            stat = 20;
            break;
          }
          *sink++ = *src++;
        break;
      }
    }
  }

  void Reader::Parser::parse(const char* block_id, char* src)
  {

  }

  Reader::Reader(std::istream& src)
  {
    read(src);
    scan();
  }

  Reader::~Reader()
  {
    delete[] pool;
  }
}
