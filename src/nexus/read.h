#pragma once
/*
* read.h, header for NEXUS parser
*/

#include <iostream>
#include <vector>
#include <map>
#include "nexus/nexus.h"

namespace nexus
{
  class Reader
  {
    public:
      class Listener
      {
        public:
          virtual bool OnSeq(const Data&) = 0;
            //enumeration stops if returns true
      };

      class Parser  // think about a plugin like system to parse blocks
      {
        public:
          virtual void parse(const char*, char*);

        private:
          Data data;

      };

      Reader(std::istream& src);
      ~Reader();
      void enumerate(Listener& li);

    private:
      char *pool = nullptr;
      long long unsigned size;
      void read(std::istream& src);
      void scan();
      char* parse_data(char*);
      unsigned seq_nr = 0;
      Data data;

  };
}

//Reader::Parser object for each block?
