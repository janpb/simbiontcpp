#pragma once
/*
*
* nexus.h struct to store the genbank data
*/

namespace nexus
{
  struct Data
  {
    unsigned len;
    //possible to create new map with same parameters but new name?
    std::map<const char*, const char*> cmds;
    std::map<unsigned, std::map<const char*, const char*>> mtx;
  };

 //class Parser  // think about a plugin like system to parse blocks
      //{
        //public:
          //virtual void parse(const char*, char*) = 0;

      //};
}
