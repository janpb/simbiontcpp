/*
 * fasta.sort.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
#include <vector>
//#include <boost/program_options.hpp>
#include <string>
#include "fasta/read.h"

std::vector<std::string> split(const std::string& s, const char c)
{
  std::string substr;
  std::string::size_type pos = 0;
  std::string::size_type prev_pos = 0;
  std::vector<std::string> sv;

  bool flag = true;

  while(flag)
  {
    pos = s.find_first_of(c, pos);
    if(pos == std::string::npos)
    {
      flag = false;
      pos = s.size();
    }
    substr = s.substr(prev_pos, pos-prev_pos);
    sv.push_back(substr);
    prev_pos = ++pos;
  }
  return sv;
}

class Sorter: public fasta::Reader::Provider
{
  public:
  //  Sorter();
  //  ~Sorter();
    void provide(sequence::Sequence*);

  private:
    std::vector<sequence::Sequence> seqveq;
    void set_seqdate(sequence::Sequence*);
};

void Sorter::set_seqdate(sequence::Sequence* seq)
{
  //std::cout << seq->id() << "\n";
  std::vector<std::string> header =  split(seq->id(), ';');
  std::string date_string;
  for(auto& i : split(header.at(2), '/'))
  {
    if(i.size() == 0){ date_string += "01"; }
    else{ date_string += i; }
  }
  //std::cout << date_string << seq->id() << "\n";
  //std::cout << std::stoi(date_string) << "++++---\n";
  seq->date = std::stoul(date_string);
  std::cout << seq->date << "\t" << seq->id() << "\n";
}

//20101216++++---
//20130114---
//20101216++++---


void Sorter::provide(sequence::Sequence* seq)
{
  set_seqdate(seq);
}

int main(int argc, char **argv)
{
  fasta::Reader reader(std::cin);
  Sorter s;
  reader.iterate(s);
  return 0;
}
