/*
 * msa2svg.cpp
 * create svg figures from multiple sequence alignements
 */

#include <iostream>
#include "fasta/read.h"
#include "svg/svg.h"
#include "sequence/seq.h"

class MSA:public fasta::Reader::Provider
{
  public:
    MSA();
    void provide(sequence::Sequence* seq);
  private:
    svg::SVG img;
    double y = 0;
    typedef std::map<char, const svg::SVG::Color> Color;
    static Color coloring;

};

  MSA::Color MSA::coloring
  {// proteins and DNA, colors: http://www.w3.org/TR/css3-color/#svg-color
    {'A', 0x000000ff},
    {'C', 0x00228b22},
    {'D', 0x0000ffff},
    {'E', 0x00a52a2a},
    {'F', 0x005f9ea0},
    {'G', 0x00ff0000},
    {'H', 0x007fff00},
    {'I', 0x006495ed},
    {'K', 0x0000ffff},
    {'L', 0x008b008b},
    {'M', 0x00ff8c00},
    {'N', 0x008b0000},
    {'P', 0x002f4f4f},
    {'Q', 0x0000bfff},
    {'R', 0x00adff2f},
    {'S', 0x00ff00ff},
    {'T', 0x00ffd700},
    {'V', 0x00191970},
    {'W', 0x00ff4500},
    {'Y', 0x00ee82ee},
    {'U', 0x00ffa500},
  };

MSA::MSA()
:img(std::cout)
{

}

void MSA::provide(sequence::Sequence* s)
{

  double x = 0;
  for(auto i = 0u; i < s->length(); ++i)
  {
    svg::SVG::Draw style = {coloring[s->seq()[i]],  {svg::SVG::NONE, 1}};
    img.rect(style, x, y, 10, 10);
    x += 12;
  }
  y += 12;
  //return false;
}

int main(int argc, char **argv)
{
  fasta::Reader reader(std::cin);
  MSA msa;
  reader.iterate(msa);
  return 0;
}
