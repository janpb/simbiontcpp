/*
 * fasta.countFreq.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Counts the frequency of nucleotides/amino acids for Seqeucnes in Fasta
 *  format
 * Version: 0
 */



#include "fasta/read.h"
#include "sequence/seq.h"
#include "scuo/scuo.h"

class Counter:public fasta::Reader::Provider
{
  public:
    void provide(sequence::Sequence*);

  private:
    //std::map<std::string, double> freqmap;
};

void Counter::provide(sequence::Sequence* s)
{
  std::cout << s->id() << "\n" << s->length() << "\n";
  scuo::SCUO sc;
  std::string seq = s->seq();
  sc.calcCodonFreq(seq);
  sc.calcShannonEntropy();
  std::cout << "\n-----------------------------\n";
}


int main(int argc, char **argv)
{
  fasta::Reader reader(std::cin);
  Counter c;
  reader.iterate(c);
  return 0;
}
