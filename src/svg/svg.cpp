/* Implementation of the svg methods defined in svg.h
*
*
*
*/

#include "svg/svg.h"
#include <iomanip>


namespace svg
{
  void SVG::color(std::ostream& out, Color c)
  {
    if(c == NONE)
    {
      out << "\"none\"";
    }
    else
    {
      out << "\"rgb(" << ((c >> 16) & 0xFF) << ","
                      << ((c >>  8) & 0xFF) << ","
                      << ((c >>  0) & 0xFF) << ")\"";
    }
  }

  void SVG::Font::svg(std::ostream& out) const
  {
      out <<  "font-size=\""   << size << "\" "
              "fill=";
        SVG::color(out, color);
  }


  void SVG::Draw::svg(std::ostream& out) const
  {
    out << "fill=";
    color(out, fill);
    out << " stroke=";
    color(out, stroke.fill);
    out << " stroke-width=\"" << stroke.width << "\"/>\n";
  }


  SVG::SVG(std::ostream& out)
  :out(out)
  {
    out << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
        << " <!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.0//EN\"\n"
        << "\"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">\n"
        << "<svg xmlns=\"http://www.w3.org/2000/svg\"\n"
        << "xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n"
        << "<!-- see  http://www.w3.org/TR/SVG11/ -->\n";
  }

  SVG::~SVG()
  {
    out << "</svg>\n";
  }

  void SVG::rect(const Draw& style, double x0, double y0, double wi, double he)
  {
    out << "<rect x= \""    << x0 << "\" "
                 "y=\""     << y0 << "\" "
                 "width=\""  << wi << "\" "
                 "height=\"" << he << "\" \n";
                 style.svg(out);
  }

  void SVG::circle(const Draw& style, double cx, double cy, double r)
  {
    out << "<circle cx=\"" << cx << "\" "
                   "cy=\"" << cy << "\" "
                   "r=\""  <<  r << "\" ";
                    style.svg(out);
  }

  void SVG::line(const Draw& style, double x0, double y0, double x1, double y1)
  {
    out << "<line x1= \"" << x0 << "\" "
                 "y1=\""  << y0 << "\" "
                 "x2=\""  << x1 << "\" "
                 "y2=\""  << y1 << "\" "
                 "stroke=\"red\" stroke-width=\"4\"/>\n";
  }

  void SVG::text(const Font& style, double x, double y, const std::string& text)
  {
    out << "<text x=\"" << x    << "\" "
                 "y=\"" << y    << "\" ";
                  style.svg(out);
    out << ">\n"
        << text << "\n"
        << "</text>\n";

  }




}
