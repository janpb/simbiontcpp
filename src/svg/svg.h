#pragma once
/*
*
*/

#include <iostream>
#include <string>
namespace svg
{
  class SVG
  {

    public:
      typedef unsigned Color; // Color is an alias for an unsigned number
      std::string family = "Verdana";
      static const Color NONE  = 0;
      static const Color RED   = 0x00FF0000;
      static const Color GREEN = 0x0000FF00;
      static const Color BLUE  = 0x000000FF;

      class Draw
      {
        public:
          Color fill;
          struct
          {
            Color fill;
            double width;
          } stroke;

        private:
          friend class SVG;
          void svg(std::ostream& out) const;

      };

      class Font
      {
        public:
          Color color;
          double size;

        private:
          friend class SVG;
          void svg(std::ostream& out) const;
      };

      SVG(std::ostream& out);
      ~SVG();
      void rect(const Draw&,
                double x0,   double y0, double wi, double he);
      void circle(const Draw&,
                  double cx, double cy, double r);
      void line(const Draw&,
                double x0,   double y0, double x1, double x2);
      void text(const Font&,
                double x, double y,  const std::string& text);



    private:
      std::ostream& out;
      static void color(std::ostream& out, Color c);
  };
}
