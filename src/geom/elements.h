#pragma once
/*
 * elements.h
 *
 * Copyright 2015 Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Structs definig the basic elements in euclidean geometry
 * Version: 0
 */


namespace euclid
{
  struct Point  // think about std::tuple
  {
    double x;
    double y;
  };

  struct Vector
  {
    double x;
    double y;
  };

  //struct Framework
  //{
    //static const Point& orig = {0, 0}; // set default origin to (0,0)
  //};

} // end namespace euclid
