/*
 * vec.cpp
 *
 * Copyright 2015 Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Implementation of vector geometry functions
 * Version: 0
 */


#include <iostream>
#include "geom/vec.h"

namespace euclid
{
  void Printer::point(const Point& p)
  {
    std::cout << "x:" <<p.x << "\ny:" << p.y << "\n";
  }

  Printer::Printer()
  {
  }

  Vector Calculator::vector(const Point& p0, const Point& p1)
  {
    Vector v;
    v.x = p1.x - p0.x;
    v.y = p1.y - p0.y;
    return v;
  }

  double Calculator::distance(const Point& p0, const Point& p1)
  {
    return sqrt( pow(( p1.x - p0.x), 2) + pow((p1.y - p0.y), 2));
  }

  double Calculator::dotProduct(const Point& p0, const Point& p1)
  {
    return (p0.x * p1.x) + (p0.y * p1.y);
  }

  Circle::Circle(const Point& c, double r)
  :c(c), r(r)
  {
  }

  void Circle::setRadius(double r)
  {
    this->r = r <= 0 ? 1 : r;
  }

  void Circle::setCenter(const Point& c)
  {
    this->c = c;
  }

  Circle::~Circle()
  {

  }

} // end namespace euclid
