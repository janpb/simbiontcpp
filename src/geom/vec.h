#pragma once
/*
 * vec.h
 *
 * Copyright 2015 Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Header file for vector geometry functions
 *
 * Version: 0
 */


#include <iostream>
#include <cmath>
#include "geom/elements.h"

namespace euclid
{
  class Printer
  {
    public:
      void point(const Point&);
      Printer();

    private:
  };

  class Calculator
  {
    public:
      Vector vector(const Point&, const Point&);
      double distance(const Point&, const Point&);
      double angle(const Point&, const Point&, const Point&);
      double dotProduct(const Point&, const Point&);

    private:

  };

  class Circle
  {
    public:
      // constructor with default values
      Circle(const Point& c = {0,0}, double r = 1);
      ~Circle();
      void setRadius(double);
      void setCenter(const Point&);
      Point c;
    protected:

      double r;
      double angle;

  };

} // end namespace euclid
