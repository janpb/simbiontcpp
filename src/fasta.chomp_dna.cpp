/*
 * fasta.chomp_dna.cpp
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */

#include <iostream>
#include "fasta/read.h"
#include "sequence/seq.h"
#include "sequence/dna.h"
#include "sequence/translate.h"

class Chomper:public fasta::Reader::Provider
{
  public:
    void provide(sequence::Sequence*);

  private:
    //void Converter::screen_init(sequence::Sequence*)
};

void Chomper::provide(sequence::Sequence* seq)
{
  std::string seqid = seq->id();
  std::string dnaseq = seq->seq();
  sequence::DNA dna(seqid, dnaseq);
  dna.chomp();
  //dna.setID(dna.id() + "_orf")
  dna.print_as_fasta();
}

int main(int argc, char **argv)
{
  fasta::Reader reader(std::cin);
  Chomper c;
  reader.iterate(c);
  return 0;
}
