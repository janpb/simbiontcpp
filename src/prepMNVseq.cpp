/* prepMNVseq.cpp
 * Copyright 2014 Jan <lejosh@members.fsf.org> (how does USYD handles this?)
 *
 * Description:
 *  formats the XML Genbank output to FASTA files
 *
 * Comments:
 *  Really cheap and dirty. Would require a frontend to allow the user to define
 * which elements from the Genbank file constitute the header.
 */

#include <iostream>
#include "genbank/read.h"


class Formatter: public gb::Parser::Listener
{
  public:
    Formatter();
    bool gbEntry(const gb::Locus& loc);

  private:

};

bool Formatter::gbEntry(const gb::Locus& loc)
{

  std::cout << ">" << loc.ref << loc.src << "\n"
            << loc.seq << "\n";

  return false;
}

Formatter::Formatter()
{

}



int main(int argc, char **argv)
{
  gb::Parser parser(std::cin);
  Formatter listener;
  parser.enumerate(listener);
  return 0;
}
