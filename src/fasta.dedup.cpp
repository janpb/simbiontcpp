/*
* fasta.split.cpp
* Copyright 2014 Jan Buchmann <lejosh@members.fsf.org>
*
* Description:
* Detects duplicated sequence entries in FASTA flat files based on the hash
* value of a sequence.
*/
#include <iostream>
#include <vector>
#include "fasta/read.h"

class Dedup:public fasta::Reader::Provider
{
  public:
    Dedup();
    void provide(sequence::Sequence*);
    void lsDup();


  private:
    std::map<long long, std::vector<std::string>> dupl;
    std::hash<std::string> hash;
    unsigned duplicates;
};

void Dedup::lsDup()
{

  std::cerr << "DUPLICATES\n";
  for(const auto& j:dupl)
  {
    if(j.second.size() == 1){continue;}
    std::cerr << j.second[0];
    for(unsigned i = 1; i < j.second.size(); ++i)
    {
      std::cerr << ":" << j.second[i];
    }
    duplicates += j.second.size() - 1;
    std::cerr << "\n";
  }
  std::cerr << "#duplicates: " << duplicates << "\n";
}

void Dedup::provide(sequence::Sequence* s)
{
  if(dupl.count(hash(s->seq())) == 0) // not seen hash/seq yet
  {
    std::cout << ">" << s->id() << "\n" << s->seq() << "\n";
    dupl[(hash(s->seq()))].push_back(s->id());
  }
  else
  {
    dupl[(hash(s->seq()))].push_back(s->id());
  }
}

Dedup::Dedup()
{

}

int main(int argc, char **argv)
{
  //std::cerr << "start\n";
  fasta::Reader reader(std::cin);
  Dedup deduplicator;
  reader.iterate(deduplicator);
  deduplicator.lsDup();
  return 0;
}
