/*
 * msa.util.cpp
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  An utility tool to analyze and get sequecnes from MSAs.
 * Version: 0
 */


#include <iostream>
#include <boost/program_options.hpp>
#include "msa/util.h"


int main(int argc, char **argv)
{
  boost::program_options::options_description desc("Filter options:");
  desc.add_options()
    ("help,h", "print usage")

    ("extract,e", boost::program_options::value<std::vector<int>>(),
     "Extract the Nth sequence(s) from the MSA into a subset.")
    ;
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);
  if(vm.count("help"))
  {
    std::cout << desc << "\n";
    return 1;
  }
  msa::MSA msa;
  msa.reader(std::cin);
  msa::Columns cols = msa.columns();
  msa::Extractor e;
  e.extract(cols);
  return 0;
}
