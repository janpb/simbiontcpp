/*
 * fasta.composition.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
#include "sequence/seq.h"
#include "sequence/composition.h"
#include "fasta/read.h"

class Analyzer:public fasta::Reader::Provider
{
  public:
    void provide(sequence::Sequence*);
    void calcAvrg();
    void printFreqs();
  private:
    std::map<char, std::vector<double>> freqmap;
    std::map<char, sequence::Amac> amac_map;
    unsigned analyzed_amacs;
    unsigned analyzed_seqs;
};

void Analyzer::provide(sequence::Sequence* s)
{
  //std::cout << s.getId() << "\n";
  sequence::Analyzer seqa;
  //seqa.calcFreqCodon(s.getSeq());
  seqa.calcFreqAmac(s->seq());

  for(const auto& i : seqa.getFreqAmac())
  {
    amac_map[i.first].occurrence += i.second.occurrence;
    analyzed_amacs += i.second.occurrence;
  }
  ++analyzed_seqs;
}

void Analyzer::calcAvrg()
{
  std::cout << "Analyzed sequences:   " << analyzed_seqs  << "\n"
            << "Analyzed amino acids: " << analyzed_amacs << "\n";
  for(const auto& i : amac_map)
  {
    //std::cout << i.first << " " << i.second.occurrence << "\n";;
    double average = i.second.occurrence / analyzed_amacs;
    //average /= analyzed_amacs;
    std::cout << i.first << ": " << i.second.occurrence << "\t" << average << "\n";
  }
}

void Analyzer::printFreqs()
{

}

int main(int argc, char **argv)
{
  fasta::Reader reader(std::cin);
  Analyzer a;
  reader.iterate(a);
  a.calcAvrg();
  return 0;
}
