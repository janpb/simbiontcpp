/*
 * fasta.dna2protein.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
#include "fasta/read.h"
#include "sequence/seq.h"
#include "sequence/dna.h"
#include "sequence/translate.h"

class Converter:public fasta::Reader::Provider
{
  public:
    void provide(sequence::Sequence*);
};

void Converter::provide(sequence::Sequence* seq)
{
  sequence::Translator t;
  std::cout << ">" << seq->id()              << "\n"
                   << t.dna2prot(seq->seq()) << "\n";
}

int main(int argc, char **argv)
{
  fasta::Reader reader(std::cin);
  Converter c;
  reader.iterate(c);
  return 0;
}
