/*
 * svg_demo.cpp
 *
 */


#include <iostream>
#include "svg/svg.h"

int main(int argc, char **argv)
{
  svg::SVG::Draw style = {svg::SVG::RED,  {svg::SVG::GREEN, 2}};
  svg::SVG::Font font  = {svg::SVG::GREEN, 20};
  svg::SVG img(std::cout);
  img.rect(style, 100, 100, 100, 100);
  img.circle({svg::SVG::RED, {svg::SVG::GREEN, 10}}, 200, 200, 50);
  img.line(style, 250, 300, 300, 350);
  img.text(font, 400, 400, "Hello SVG");
  return 0;
}

