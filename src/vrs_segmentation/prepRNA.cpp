/*
 * prepRNA.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Prepare mrna reads for analysis by creating overlapping subsequences
 * Version: 0
 */


#include <iostream>
#include <iostream>
#include "fasta/read.h"
#include "sequence/sequences.h"

class Slicer:public fasta::Reader::Provider
{
  public:
    Slicer();
    void provide(sequence::Sequence* seq);

  private:
    void mkSubseqs(sequence::Sequence* seq, unsigned win_len = 500,
                                            unsigned shift = 250);
};

void Slicer::provide(sequence::Sequence* seq)
{
  mkSubseqs(seq);
}

void Slicer::mkSubseqs(sequence::Sequence* seq, unsigned win_len,
                                                unsigned shift)
{

  for(unsigned i; i < seq->length(); i += shift)
  {
    //if(i + win_len > seq->length())
      //{ win_len = seq->length() - i; }
    //std::cout << seq->length() - i   << "\n";
    std::cout << i << " " << win_len << " " << "\n";
  }
}

Slicer::Slicer()
{

}


int main(int argc, char **argv)
{
  fasta::Reader r(std::cin);
  Slicer s;
  r.iterate(s);
  return 0;
}
