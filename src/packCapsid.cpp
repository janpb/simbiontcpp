/*
 * packCapsid.cpp
 *
 * Copyright 2015 Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */

#include <iostream>
#include "geom/vec.h"


class Capsid: public euclid::Circle
{
  public:
    Capsid(const euclid::Point& c = {0,0}, double r = 10): Circle(c, r) // initializer list
    {
    }
    ~Capsid();  // not inherited
    void filler();
  private:

};

class Genome: public euclid::Circle
{
  public:
    Genome(const euclid::Point& c = {0,0}, double r = 10): Circle(c, r) // initializer list
    {
    }
    ~Genome();  // not inherited
  private:

};

void Capsid::filler()
{
  double rGenome = 1.5;
  double dGenome = 2 * rGenome;
  double i = rGenome;
  double j = rGenome;
  int count_genomes = 0;
  euclid::Printer lp;
  while(true)
  {
    Genome g({i, j}, rGenome);
    if(i >= r)
    {
      i  = rGenome;
      j += dGenome;
    }
    if(j >= r)
    {
      break;
    }
     std::cout << "Genome: " << count_genomes << "\n";
     lp.point(g.c);
    ++count_genomes;
    i += dGenome;
  }

}

Capsid::~Capsid()
{

}

Genome::~Genome()
{

}

int main(int argc, char **argv)
{
  Capsid cp({0,0}, 10);
  cp.filler();
  return 0;
}
