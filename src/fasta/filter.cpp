/*
 * filter.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <"fasta/filter.h">
namespace fasta
{
  Filter::Filter()
    {}
  Filter::~Filter()
    {}

 Filtration::Filtration(boost::program_options::variables_map& vm)
  {
    add_filter(vm);
  }
  Filtration::~Filtration()
  {
    for(auto& i : fveq)
    {
      delete i;
    }
  }

  void Filtration::add_filter(boost::program_options::variables_map& vm)
  {
    if(vm.count("length"))
    {
      FilterLen* f = new FilterLen(vm["length"].as<std::string>());
      fveq.push_back(f);
    }
    if(vm.count("char"))
    {
      CompositionFilter* cf = new CompositionFilter(vm["char"].as< std::vector<std::string> >());
      fveq.push_back(cf);
    }
    // add rm of non-IUPAC chars
  }

  void Filtration::provide(sequence::Sequence* seq)
  {
    bool reject = false;
    for(auto &i : fveq)
    {
      if(i->filter(seq) == true)
      {
        reject = true;
        break;
      }
    }
    if(reject == false)
    {
      std::cout << ">" << seq->id()  << "\n"
                       << seq->seq() << "\n";
    }
  }
} //end namespace fasta
