/*
 * diff.cpp
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
#include <typeinfo>
#include "fasta/diff.h"


namespace fasta
{
  Differ::Differ(int argc, char** argv)
  {
    parse_options(argc, argv);
    collect();
  }
  Differ::~Differ()
    {  }

  void Differ::parse_options(int argc, char** argv)
  {
    po::options_description desc("USAGE\nShow differences between FASTA files");
    desc.add_options()
      ("help,h",    "print usage")

      ("file,f",    po::value<std::vector<std::string>>(),
       "FASTA file(s) to diff")

      ("headers,i", po::bool_switch()->default_value(false),
       "Diff sequence headers")

      ("seq,s", po::bool_switch()->default_value(true),
       "Diff sequences")
      ;
    po::positional_options_description p;
    p.add("file", -1);
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
      std::cout << desc << "\n";
      std::exit(0);
    }

    if(vm.count("headers") and vm["headers"].as<bool>() == true)
      { printHeaders = true; }

    if(vm.count("seqs") and vm["seqs"].as<bool>() == true)
      { printSeqs = true; }
  }

  void Differ::collect()
  {
    Collector cltor(diff_id, diff_seq, diffvec);
    if(vm.count("file"))
    {
      for(auto& i : vm["file"].as< std::vector<std::string> >())
      {
        fasta::Reader reader(i);
        cltor.set_src(i);
        add_src(i.substr(i.find_last_of("/") + 1));
        reader.iterate(cltor);
      }
    }
    else
    {
      std::cerr << "No input files. Trying stdin.\n";
      fasta::Reader reader(std::cin);
      reader.iterate(cltor);
    }
  }

  void Differ::print_headers()
  {
    std::cerr << "Duplicate headers:\n";
    for(auto& i : diff_id)
    {
      if(i.second.size() > 1)
      {
        std::cout << get_entry_id(i.second[0]) << "\t";
        for(unsigned j = 1; j < i.second.size(); ++j)
          { std::cout << get_entry_seqnum(i.second[j])  << "\t"; }
        std::cout << "\n";
      }
    }
  }

  void Differ::print_sequences()
  {
    std::cerr << "Duplicate sequences:\n";
    for(auto& i : diff_seq)
    {
      if(i.second.size() > 1)
      {
        std::cout << get_entry_id(i.second[0])     << ":" <<
                     get_entry_seqnum(i.second[0]) << "\t";
        for(unsigned j = 1; j < i.second.size(); ++j)
        {
          std::cout << get_entry_id(i.second[j])     << ":" <<
                       get_entry_seqnum(i.second[j]) << "\t";
        }
        std::cout << "\n";
      }
    }
  }

  void Differ::print_srcs()
  {
    for(auto& i : srcs)
      { std::cout << "#" << i << "\t"; }
    std::cout << "\n";
  }


  void Differ::summarize()
  {
    print_srcs();
    if(printHeaders)
      { print_headers(); }

    if(printSeqs)
      { print_sequences(); }
  }

  std::string Differ::get_entry_id(unsigned idx)
  {
    return diffvec[idx].id;
  }

  std::string Differ::get_entry_src(unsigned idx)
  {
    return diffvec[idx].src;
  }

  std::string Differ::get_entry_path(unsigned idx)
  {
    return diffvec[idx].path;
  }

  unsigned Differ::get_entry_seqnum(unsigned idx)
  {
    return diffvec[idx].num;
  }

  void Differ::show_entry(unsigned idx)
  {
    std::cout << diffvec[idx].num << " ";
  }

  void Differ::add_src(std::string src)
  {
    srcs.push_back(src);
  }

  void Differ::show()
  {
    for(auto& i : diffvec)
    {
      std::cout << "showing: " << i.id << ":" << i.src << ":" << i.num << "\n";
    }
  }

  Differ::Collector::Collector(diffmap& ref_diff_id,
                               diffmap& ref_diff_seq,
                               std::vector<Differ::Diffentry>& diffvec)
  :diff_id(ref_diff_id), diff_seq(ref_diff_seq), diffvec(diffvec)
    { }



  // ToDo: identify duplicated per source already here
  void Differ::Collector::provide(sequence::Sequence* s)
  {
    unsigned idx_diffvec = diffvec.size();
    Differ::Diffentry e ({s->id(), src.substr(src.find_last_of("/") + 1), src,
                          seq_count});
    //add_src(src);
    diffvec.push_back(e);
    diff_id[s->get_idhash()].push_back(idx_diffvec);
    diff_seq[s->get_seqhash()].push_back(idx_diffvec);
    ++seq_count;
  }

  void Differ::Collector::set_src(std::string src)
  {
    this->src = src;
    seq_count = 0;
  }

} //end namespace fasta
