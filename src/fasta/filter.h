#pragma once
/*
 * filter.h
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
#include <boost/program_options.hpp>
#include <string>
#include "fasta/read.h"
#include "sequence/composition.h"

namespace fasta
{
  class Filter
  {
    public:
      Filter();
      virtual ~Filter();
      virtual bool filter(sequence::Sequence*) = 0;

    private:


  };

  class Filtration:public fasta::Reader::Provider
  {
    public:
      Filtration(boost::program_options::variables_map& vm);
      ~Filtration();
      void provide(sequence::Sequence*);

    private:
      void set_moltype(std::string);
      void add_filter(boost::program_options::variables_map& vm);
      std::vector<Filter*> fveq;
      std::string moltype = "nt";
  };
} //end namespace fasta
