#pragma once
//header
#include <iostream>
#include <fstream>
#include <string>
#include "sequence/seq.h"
namespace fasta
{
  class Writer
  {
    public:
      Writer();
      void write(const std::string& seq,
                       std::string fname = "sequence.fa" /* default output name */,
                       std::string id = "Sequence");
    private:
      void fmtSeq(std::ofstream&,
                  const std::string& seq,
                  unsigned width = 50 /* default line length */);
  };
}
