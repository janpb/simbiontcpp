/*
 * fasta/read.cpp
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Implementation of the Fasta file reader.
 *
 * ToDo:
 *  - Not deleting the pointers to new Seqeunce. Does the vector
 *    seqcev takes care about the memory management? How do I test this?
 *  - Replace the first step of loading the whole file into an array.
 *    Think about reading chunks of the file, process it, read anothert chunk
 *    and stil be able to use pointers
 */
#include "fasta/read.h"

namespace fasta
{
  void Reader::iterate(Provider& p)
  {
    for(auto& i : seqvec)
    {
      p.provide(i);  // passing sequence object as pointer
    }
  }

  void Reader::read(std::istream& src)
  {
    std::ios_base::sync_with_stdio(false);
    std::vector<char> fil;
    char buffer[4 * 1024];
    std::cerr << "start\n";
    while(true)
    {
      src.read(buffer, sizeof(buffer));
      unsigned len = src.gcount();
      for(auto i = 0u; i < len; ++i)
      {
        fil.push_back(buffer[i]);
      }
      if(len < sizeof(buffer)){break;}
    }
    size = fil.size();
    std::cerr << "finish\n";
    pool = new char[size+1];
    for(auto i = 0u; i < size; ++i)
    {
      pool[i] = fil[i];
    }
    pool[size] = '\0';
  }

  void Reader::read(const std::string& fname)
  {
    // Found this solution on the Internet (where else). Had no clue such
    // functions exist
    std::cerr << "start\n";
    std::ifstream ifs(fname, std::ifstream::in);
    std::filebuf* pbuf = ifs.rdbuf();
    size = pbuf->pubseekoff(0, ifs.end, ifs.in);
    pbuf->pubseekpos(0, ifs.in);
    pool = new char[size + 1];
    pbuf->sgetn(pool, size);
    pool[size] = '\0';
    ifs.close();
    std::cerr << "finish\n";
  }

  void Reader::parse()
  {
    char* header = nullptr;
    unsigned status = 0;
    char* src = pool;
    char* dst = nullptr;
    char* seq = nullptr;
    for(auto i = 0u; i <= size; ++i) // <= b/c of '\0'
    {
      switch(status)
      {
        case 0:
          if(*src == '>')
          {
            status = 1;
            ++src;
            header = src;
            break;
          }
        return; // error

        case 1:
          if(*src == '\n')
          {
            status = 2;
            *src++ = '\0';
            dst = src;
            seq = src;
            break;
          }
          ++src;
        break;

        case 2:
          if(*src == '\n')
          {
            *dst = '\0';
            ++src;
            status = 3;
            break;
          }
          *dst++ = *src++;
        break;

        case 3:
          if(*src == '>')
          {
            sequence::Sequence* s = new sequence::Sequence(header, seq); //pointer to new instance of class Seqeunce
            seqvec.push_back(s);
            status = 1;
            header = ++src;
            break;
          }
          if(*src == '\n')
          {
            ++src;
            break;
          }
          if(*src== '\0')
          {
            sequence::Sequence* s = new sequence::Sequence(header, seq); //pointer to new instance of class Seqeunce
            seqvec.push_back(s);
            *dst = '\0';
            return;
          }  // end of array | error
          status = 2;
          *dst++ = *src++;
        break;
      }
    }
  }

  Reader::Reader(std::istream& src)
  {
    read(src);
    parse();
  }

  Reader::Reader(const std::string& src)
  {
    read(src);
    parse();
  }

  Reader::~Reader()
  {
    for(auto& i : seqvec)
    {
      delete i;
    }
    delete [] pool;
  }
}// end namespace fasta
