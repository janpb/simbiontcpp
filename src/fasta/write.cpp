//header

#include "fasta/write.h"

namespace fasta
{
  Writer::Writer()
    { }

    void Writer::fmtSeq(std::ofstream& fout,
                        const std::string& seq,
                        unsigned width)
    {
      unsigned pos = 0;
      for(auto& i : seq)
      {
        if(pos % width == 0 && pos > 0){fout << "\n";}
        fout << i;
        ++pos;
      }
      fout << "\n\n";
    }

    void Writer::write(const std::string& seq,
                             std::string fname,
                             std::string id)
    {
      std::ofstream fout;
      //std::cout << seq << " :" << fname << ": " << id << "\n";
      fout.open(fname);
      fout << ">" << id << "\n";
      fmtSeq(fout, seq);
      fout.close();
    }
}// end namespace fasta
