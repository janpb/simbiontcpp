#pragma once
/*
 * diff.h
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
#include <boost/program_options.hpp>
#include "fasta/read.h"

namespace po = boost::program_options;  // not very best practice
namespace fasta
{
  class Differ
  {
    public:
      Differ(int argc, char** argv);
      ~Differ();
      void parse_options(int, char**);
      void show();
      void summarize();
      void add_src(std::string);
      void print_headers();
      void print_sequences();

      std::string get_entry_id(unsigned);
      std::string get_entry_src(unsigned);
      std::string get_entry_path(unsigned);
      unsigned get_entry_seqnum(unsigned);

      struct Diffentry
      {
        std::string id;
        std::string src;
        std::string path;
        unsigned num;
      };

    private:
      void collect();
      void show_entry(unsigned);
      void print_srcs();

      bool printSeqs = true;
      bool printHeaders = false;
      std::vector<std::string> srcs;
      typedef std::map< unsigned long, std::vector<unsigned> > diffmap;
      std::vector<Differ::Diffentry> diffvec;
      po::variables_map vm;
      diffmap diff_id;
      diffmap diff_seq;

      class Collector: public fasta::Reader::Provider
      {
        public:
          Collector(diffmap&,
                    diffmap&,
                    std::vector<Differ::Diffentry>&);

          void provide(sequence::Sequence*);
          void set_src(std::string);

        private:
          std::string src = "stdin";
          diffmap& diff_id;
          diffmap& diff_seq;
          std::vector<Differ::Diffentry>& diffvec;
          unsigned seq_count = 0;
      };
  };
} //end namespace fasta
