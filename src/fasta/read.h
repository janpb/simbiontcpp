#pragma once
/*
 * fasta/read.h
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 *
 * Description: Header file for Fasta reader.
 * ToDo:
 *  - Check Iterators: http://www.cplusplus.com/reference/iterator/iterator/
 *  - Check smart_pointers: e.g. unique_ptr, shared_ptr
 */
#include <iostream>
#include <vector>
#include <fstream>
#include "sequence/seq.h"

namespace fasta
{
  class Reader
  {
    public:
      class Provider
      {
        public:
          virtual void provide(sequence::Sequence*) = 0;
      };
      Reader(std::istream& src);
      Reader(const std::string&);
      ~Reader();
      void iterate(Provider&);


    private:
      char *pool = nullptr;
      long long unsigned size;
      void read(std::istream& src);
      void read(const std::string&);
      void parse();
      std::vector<sequence::Sequence*> seqvec;
  };
} // end namespace fasta
