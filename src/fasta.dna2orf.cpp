/*
 * fasta.dna2orf.cpp
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */

#include <iostream>
#include "fasta/read.h"
#include "sequence/seq.h"
#include "sequence/dna.h"
#include "sequence/translate.h"

class Converter:public fasta::Reader::Provider
{
  public:
    void provide(sequence::Sequence*);

  private:
    //void Converter::screen_init(sequence::Sequence*)
};

void Converter::provide(sequence::Sequence* seq)
{
  std::string seqid = seq->id();
  std::string dnaseq = seq->seq();
  sequence::DNA dna(seqid, dnaseq);
  int len = 0;
  int beg = 0;
  for(auto& i : dna.get_init_positions())
  {
    if(i.second > len)
    {
      len = i.second;
      beg = i.first;
    }
  }
  std::string orf = dna.subseq(beg, beg+len);
  dna.setSeq(orf);
  dna.chomp();
  //dna.setID(dna.id() + "_orf")
  dna.print_as_fasta();
}

int main(int argc, char **argv)
{
  fasta::Reader reader(std::cin);
  Converter c;
  reader.iterate(c);
  return 0;
}
