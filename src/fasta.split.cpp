/*
* fasta.split.cpp
* Copyright 2015 The University of Sydney
* Author: Jan P Buchmann <lejosh@members.fsf.org>
*
* Description:
*  Splitting FASTA flat files into sepearte FASTA files.
*
* version: 1
*/
#include <iostream>
#include <map>
#include <boost/program_options.hpp>
#include "fasta/read.h"
#include "fasta/write.h"

namespace po = boost::program_options;  // not very best practice

class Split:public fasta::Reader::Provider
{
  public:
    Split(int argc, char** argv);
    ~Split();
    void parse_options(int, char**);
    void provide(sequence::Sequence*);

  private:
    const std::string setFilename(std::string);
    std::map<std::string, int> ids;
    po::variables_map vm;
};

Split::Split(int argc, char** argv)
  { parse_options(argc, argv); }

void Split::parse_options(int argc, char** argv)
{
    po::options_description desc("USAGE\nSplit FASTA flat files");
    desc.add_options()
      ("help,h", "print usage")
      ("id,i",  po::value<std::vector<std::string>>(), "FASTA seq ids")
      ;
    po::positional_options_description p;
    p.add("id", -1);
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
      std::cout << desc << "\n";
      std::exit(0);
    }

    if(vm.count("id"))
    {
      for(auto& i : vm["id"].as< std::vector<std::string> >())
        { ids[i] = 1; }
    }
}

Split::~Split()
  { }

void Split::provide(sequence::Sequence* seq)
{
  std::string seqname;
  for(auto& i : seq->id())
  {
    if(i == ' ')
      { break; }
    seqname += i;
  }
  if(ids.size() > 0 && ids.count(seqname) > 0)
  {
    std::cout << "Found " << seq->id() << "\n";
    std::string fname = setFilename(seq->id()) + ".fa";
    fasta::Writer w;
    w.write(seq->seq(), fname, seq->id());
  }
  if(ids.size() == 0)
  {
    std::string fname = setFilename(seq->id()) + ".fa";
    fasta::Writer w;
    w.write(seq->seq(), fname, seq->id());
  }
  //std::cout << ids.size() << "\n";
}

const std::string Split::setFilename(std::string fname)
{
  std::string fn;
  for(auto& i : std::string(fname))
  {
    if(i == '|'){ continue; }
    if(i == '/'){ i = '_';}
    if(i == ' '){ return fn; }
    fn += i;
  }
  std::cout << fn << "\n";
  return fn;
}


int main(int argc, char **argv)
{
  Split splitter(argc, argv);
  fasta::Reader reader(std::cin);
  reader.iterate(splitter);
  return 0;
}
