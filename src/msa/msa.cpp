/*
 * msa.cpp
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
#include "msa/msa.h"
namespace msa
{
  void MSA::reader(std::istream& src)
  {
    MSA::Converter msa_conv(msa, msa_cols, msa_rows);
    fasta::Reader reader(src);
    reader.iterate(msa_conv);
  }

  Cell* MSA::get_cell(unsigned col, unsigned row)
  {
    // cell = (row * (length_msa + 1)) + length_msa;
    // length_msa + 1: compensate for sequence header
    return& msa[(row*(msa_cols+1))+col];
  }

  const std::vector<unsigned> MSA::get_size()
  {
    return std::vector<unsigned> {msa_cols, msa_rows};
  }

  Columns MSA::columns()
  {
    Columns cols;
    for(unsigned i = 1; i <= msa_cols; ++i)
    {
      Column c;
      for(unsigned j = 0; j < msa_rows; ++j)
      {
        c.add_cell(get_cell(i, j));
      }
      cols.add_column(c);
    }
    return cols;
  }

  Rows MSA::rows()
  {
    Rows rows;
    for(unsigned i = 0; i < msa_rows; ++i)
    {
      Row r(get_cell(0, i)->value);
      for(unsigned j = 1; j <= msa_cols; ++j)
      {
        r.add_cell(get_cell(j, i));
      }
      rows.add_row(r);
    }
    return rows;
  }

  MSA::Converter::Converter(std::vector<Cell>& msa,
                            unsigned& msa_cols,
                            unsigned& msa_rows)
  : msa(msa), msa_cols(msa_cols), msa_rows(msa_rows)
  { }

  MSA::Converter::~Converter()
  { }

  void MSA::Converter::provide(sequence::Sequence* s)
  {
    if(msa_rows == 0)
    {
      msa_cols = s->length();
    }

    if(s->length() != msa_cols)
    {
      std::cout << "Length Error: " << s->id() << "(" << msa_rows << ")\n";
      std::exit(0);
    }

    msa.push_back(Cell {s->id(), msa_rows, -1, -1});
    int pos_msa = 0;
    int pos_seq = 0;
    for(auto& i : s->subseq(0,0,1))
    {
      if(i != '-')
        { ++pos_seq; }
      std::string v;
      v = i;
      msa.push_back(Cell {v, msa_rows, pos_msa, pos_seq});
      ++pos_msa;
    }
    ++msa_rows;
  }
} //end namespace msa
