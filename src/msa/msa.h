#pragma once
/*
 * msa.h
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
#include <vector>
#include "fasta/read.h"
#include "msa/vectors.h"
//#include "msa/msa.h"

namespace msa
{
  class MSA
  {
    public:
      void reader(std::istream&);
      const std::vector<unsigned> get_size();
      Cell* get_cell(unsigned/*col*/, unsigned/*row*/);
      Columns columns();
      Rows rows();

    private:
      class Converter:public fasta::Reader::Provider
      {
        public:
          Converter(std::vector<Cell>&, unsigned&, unsigned&);
          ~Converter();
          void provide(sequence::Sequence*);

        private:
          std::vector<Cell>& msa;
          unsigned& msa_cols;
          unsigned& msa_rows;
      };
      std::vector<Cell> msa;
      unsigned msa_cols = 0;
      unsigned msa_rows = 0;
  };
}// end namespace msa
