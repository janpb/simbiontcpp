#pragma once
/*
*
*/
#include <map>
#include "svg/svg.h"
#include "fasta/read.h"
#include "fasta/seq.h"


namespace msa
{
  class Graph:public fasta::Reader::Listener
  {
    public:
      class Listener
      {
        public:
        virtual bool onSeq(const fasta::Seq&) = 0; //enumeration stops if returns true
      };

    private:
      typedef std::map<char, svg::SVG::Color> Color;
      const static Color coloring;
  };
}
