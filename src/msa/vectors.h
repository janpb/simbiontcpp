#pragma once
/*
 * vectors.h
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
#include <vector>

namespace msa
{
/* -------------------- Basic MSA units and vectors -------------------- */
  struct Cell
  {
    std::string value;
    unsigned row;
    int pos_msa; // implies column as well
    int pos_seq;
  };

  class MSAVector
  {
    public:
      virtual void add_cell(Cell*);
      virtual void print();
      std::map<std::string, int>& get_valmap();
      int get_size();
      std::vector<Cell*>& get_cells();

    private:
      std::vector<Cell*> cells;
      std::map<std::string, int> valmap;
  };

/* -------------------- Columns -------------------- */
  class Column: public MSAVector
  { };

  class Columns
  {
    public:
      void add_column(Column);
      void print();
      std::vector<Column>& get_columns();

    private:
      std::vector<Column> columns;
  };

/* -------------------- Rows -------------------- */
  class Row: public MSAVector
  {
    public:
      Row(std::string&);
      void set_id(std::string&);
      const std::string& get_id();

    private:
      std::string id;
  };

  class Rows
  {
    public:
      void add_row(Row);
      void print();
      std::vector<msa::Row>& get_rows();

    private:
      std::vector<msa::Row> rows;
  };
} //end namespace msa
