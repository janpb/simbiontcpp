/*
 * vectors.cpp
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
#include <map>
#include "msa/vectors.h"

namespace msa
{
  void MSAVector::add_cell(Cell* cell)
  {
    cells.push_back(cell);
  }

  void MSAVector::print()
  {
    for(auto& i : cells)
    {
      std::cout << i->value;
    }
    std::cout << "\n";
  }

  std::map<std::string, int>& MSAVector::get_valmap()
  {
    for(auto& i : cells)
    {
      ++valmap[i->value];
    }
    return valmap;
  }

  int MSAVector::get_size()
  {
    return cells.size();
  }

  std::vector<Cell*>& MSAVector::get_cells()
  {
    return cells;
  }

  void Columns::add_column(Column c)
  {
    columns.push_back(c);
  }

  std::vector<Column>& Columns::get_columns()
  {
    return columns;
  }

  void Columns::print()
  {
    for(auto& i : columns)
    {
      i.print();
    }
  }

  Row::Row(std::string& id)
  :id(id)
   { }

  void Rows::add_row(Row r)
  {
    rows.push_back(r);
  }

  std::vector<msa::Row>& Rows::get_rows()
  {
    return rows;
  }

  void Rows::print()
  {
    for(auto& i : rows)
    {
      std::cout << i.get_id() << ": ";
      i.print();
    }
  }



  void Row::set_id(std::string& id)
  {
    this->id = id;
  }

  const std::string& Row::get_id()
  {
    return id;
  }
}//end namespace msa
