/*
 * msa_graph.cpp
 *
 * Implementation to create svg figures from multiple sequence alignments
 */


#include <iostream>
#include "msa/msa_graph.h"


namespace msa
{
  const Graph::Color Graph::coloring
  {// proteins and DNA, colors: http://www.w3.org/TR/css3-color/#svg-color
    {"A", 0x000000ff},
    {"C", 0x00228b22},
    {"D", 0x0000ffff},
    {"E", 0x00a52a2a},
    {"F", 0x005f9ea0},
    {"G", 0x00ff0000},
    {"H", 0x007fff00},
    {"I", 0x006495ed},
    {"K", 0x0000ffff},
    {"L", 0x008b008b},
    {"M", 0x00ff8c00},
    {"N", 0x008b0000},
    {"P", 0x002f4f4f},
    {"Q", 0x0000bfff},
    {"R", 0x00adff2f},
    {"S", 0x00ff00ff},
    {"T", 0x00ffd700},
    {"V", 0x00191970},
    {"W", 0x00ff4500},
    {"Y", 0x00ee82ee},
    {"U", 0x00ffa500},
  };
}

