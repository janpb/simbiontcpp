#pragma once
/*
 * util.h
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>

#include "msa/msa.h"
#include "msa/vectors.h"

namespace msa
{
  class Extractor
  {
    public:
      std::string extract(msa::Columns&);
    private:
  };
} //end namespace msa
