/*
 * fasta.diff.cpp
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Tool to identify differences within or between fasta sequence files.
 * Version: 0
 */


#include <iostream>
#include "fasta/diff.h"


int main(int argc, char** argv)
{
  fasta::Differ d(argc, argv);
  d.summarize();
  //d.show();
}
