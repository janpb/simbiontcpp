/*
 * msa.toFasta.cpp
 *
 * Copyright 2015 Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 * msa.toFasta.cpp reads an MSA file in FASTA format and returns the
 * individual sequences in FASTA format
 * Version: 0
 */

#include <iostream>
#include "fasta/read.h"
#include "sequence/seq.h"

class Split:public fasta::Reader::Provider
{
  public:
    Split();
    void provide(sequence::Sequence* seq);

  private:
    const char* rmGaps(char*);
    const char* mkHeader(char*);
};


const char* Split::rmGaps(char* seq_msa)
{
  char* sink = seq_msa;
  for(char* c = seq_msa; *seq_msa; ++c)
  {
    if(*c == '-')
    {
      *sink++ = *c;
    }
  }
  std::cout << seq_msa << "\n";
  return seq_msa;
}

const char* Split::mkHeader(char* header_msa)
{
  for(char* c = header_msa; *header_msa; ++c)
  {
    if(*c == ' ')
    {
      *header_msa = '\0';
      std::cout << header_msa << "\n";
      return header_msa;
    }
  }
}

void Split::provide(sequence::Sequence* seq)
{
  mkHeader(seq->id());
  rmGaps(seq->seq());
}

Split::Split()
{

}

int main(int argc, char **argv)
{
  fasta::Reader reader(std::cin);
  Split splitter;
  reader.iterate(splitter);
  return 0;
}
