/*
 * fasta.subseq.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */
#include <iostream>
#include <sstream>
#include "fasta/read.h"

class Subseq:public fasta::Reader::Provider
{
  public:
    Subseq(char*, char*);


  private:
    void provide(sequence::Sequence*);
    unsigned from;
    unsigned to;
};


void Subseq::provide(sequence::Sequence* seq)
{
  //std::cout << from << "   " << to << "\n";
  std::cout << ">" <<seq->id()  << "\n";
  //std::cout << from << " " << to << "\n";
  std::cout << seq->subseq(from, to) << "\n";
}

Subseq::Subseq(char* beg, char* end)
{
  std::stringstream strValue;
  strValue << beg;
  strValue >> from;

  std::stringstream strValue2;
  strValue2 << end;
  strValue2 >> to;
}

int main(int argc, char **argv)
{

  fasta::Reader reader(std::cin);
  Subseq sseq(argv[1], argv[2]);
  reader.iterate(sseq);
  return 0;
}
