/*
 * detect.ORF.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
#include "fasta/read.h"
#include "sequence/sequences.h"
#include "sequence/translate.h"

class Detector:public fasta::Reader::Provider
{
  public:
    void provide(sequence::Sequence*);

  private:
};

void Detector::provide(sequence::Sequence* seq)
{
  std::string id = seq->id();
  std::string dnaseq = seq->subseq();
  sequence::DNA* dna = new sequence::DNA(id, dnaseq);

  unsigned orf_count = 0;
  unsigned start = dna->findStart();
  while(start != 0)
  {
    unsigned stop = dna->findStop(start);
    if(stop != 0)
    {
      if( (stop - start) > 200)
      {
        std::cout << ">" << dna->id() << "_ORF" << orf_count << "_"
                  << start << "_" << stop << "\n";
        std::cout << dna->subseq(start, stop + 3) << "\n\n";
        ++orf_count;
      }
      start = dna->findStart(start + 1);
    }
  }
  delete dna;
}

int main(int argc, char **argv)
{
  fasta::Reader reader(std::cin);
  Detector detector;
  reader.iterate(detector);
  return 0;
}
