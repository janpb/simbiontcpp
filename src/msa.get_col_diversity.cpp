/*
 * msa.read.cpp
 *
 * Copyright 2016 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Calculats the change of informatiom for each column in the MSA based on
 *  Shannon's entropy. H_before is assumed a navie state fo equiprobable
 *  characters, e.g. log2(1/21) for amino acids.
 * Version: 0
 */


#include <iostream>
#include <iomanip>
#include <math.h>
#include <map>
#include <vector>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "msa/msa.h"


struct Scores
{
  int pos;
  double entropy;
  double chartypes;
  double switches;
};

struct ScoreLimits
{
  std::vector<double> entropy   {0, 0};
  std::vector<double> chartypes {0, 0};
  std::vector<double> switches  {0, 0};
} sl;

double normalize(double value, double min, double max)
{
  return (value - min) / (max - min);
}

int main(int argc, char **argv)
{
  msa::MSA msa;
  msa.reader(std::cin);
  msa::Columns cols = msa.columns();

  std::vector<Scores*> colscores;
  int pos = 0;
  int analyzed_cols = 0;
  double h_max_na = 1 / 21.0;
  //double h_max_nt = 1/4.0;
  for(auto& i : cols.get_columns() )
  {
    ++pos;
    std::map<std::string, int>& vmap = i.get_valmap();
    double size = double(i.get_size());
    if((vmap.count("-") == 1) && ( (vmap["-"] / size) > 0.5))
    {
      continue;
    }
    ++analyzed_cols;
    colscores.push_back(new Scores{pos, 0, double(vmap.size()), -1});
    std::string prev_value = "";
    for(auto& j : i.get_cells())
    {
      if(j->value == "-")
      {
        --size;
        continue;
      }
      if(j->value != prev_value)
      {
        colscores.back()->switches += 1;
      }
      prev_value = j->value;
    }
    //std::cout << "Col: " << pos << "\tsize: " << size << "\n";
    for(auto& j : vmap)
    {
      if(j.first == "-")
      {
        --colscores.back()->chartypes;
        continue;
      }
      double mle = j.second / size;
      //double h = j.second * std::log2(mle);
      //double h = mle * std::log2(mle/h_max_na);
      double h = mle * std::log2(mle);
      colscores.back()->entropy += std::fabs(h);
      //std::cout << "\tchar: "  << j.first << "\tmle: " << mle << "\th: " << h  << "\tfreq: " << j.second << "\tchars: " <<  colscores.back()->chartypes  << "\n";
    }
    colscores.back()->entropy = h_max_na - colscores.back()->entropy;
    //std::cout << "\thsum: " << colscores.back()->entropy << "\n";
    if(analyzed_cols == 1)
    {
      sl.entropy = {colscores.back()->entropy, colscores.back()->entropy};
      sl.chartypes = {colscores.back()->chartypes, colscores.back()->chartypes};
      sl.switches = {colscores.back()->switches, colscores.back()->switches};
      continue;
    }

    if(colscores.back()->entropy < sl.entropy[0])
    {
      sl.entropy[0] = colscores.back()->entropy;
    }
    if(colscores.back()->entropy > sl.entropy[1])
    {
      sl.entropy[1] = colscores.back()->entropy;
    }

    if(colscores.back()->switches < sl.switches[0])
    {
      sl.switches[0] = colscores.back()->switches;
    }
    if(colscores.back()->switches > sl.switches[1])
    {
      sl.switches[1] = colscores.back()->switches;
    }

    if(colscores.back()->chartypes < sl.chartypes[0])
    {
      sl.chartypes[0] = colscores.back()->chartypes;
    }
    if(colscores.back()->chartypes > sl.chartypes[1])
    {
      sl.chartypes[1] = colscores.back()->chartypes;
    }
  }

  rapidjson::Document d;
  d.SetObject();
  rapidjson::Value chartypes(rapidjson::kArrayType);
  rapidjson::Value entropy(rapidjson::kArrayType);
  rapidjson::Document::AllocatorType& allocator = d.GetAllocator();
  for(auto& i : colscores)
  {
    chartypes.PushBack(normalize(i->chartypes, sl.chartypes[0], sl.chartypes[1]), allocator);
    entropy.PushBack(normalize(i->entropy, sl.entropy[0], sl.entropy[1]), allocator);
    //double entropy_norm = normalize(i->entropy, sl.entropy[0], sl.entropy[1]);
    //double chartypes_norm = normalize(i->chartypes, sl.chartypes[0], sl.chartypes[1]);
    //std::cout << i->pos << "\t" << i->entropy   <<  "\t" << entropy_norm << "\n"
              //<<           "\t" << i->chartypes <<"\t" << chartypes_norm << "\n\n";
  }
  d.AddMember("chartypes", chartypes, allocator);
  d.AddMember("entropy", entropy,  allocator);
  rapidjson::StringBuffer strbuf;
  rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
  d.Accept(writer);
  std::cout << strbuf.GetString() << "\n";

  //std::cout << sl.chartypes[0] << "\t" << sl.chartypes[1] << "\n" <<
               //sl.entropy[0]   << "\t" << sl.entropy[1]   << "\n";
  return 0;
}
