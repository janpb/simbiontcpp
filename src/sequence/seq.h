#pragma once
/*
 * fasta/seq.h
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 *
 * Description:
 *  Header file for the Sequence class.
 */
#include <algorithm>
#include <iostream>
#include <string>
#include <map>

namespace sequence
{
  class Sequence
  {
    public:
      //  Con/De-structors
      Sequence(char* id, char* seq, std::string type = "seq");
      Sequence(std::string& id, std::string& seq, std::string type = "seq");
      virtual ~Sequence();

      // Functions
      virtual const std::string& id();
      virtual const std::string& seq();
      virtual const std::string& type();
      virtual const unsigned length();
      void setSeq(std::string& seq);
      void setId(std::string& id);
      unsigned date;
      std::string subseq(unsigned from = 0, unsigned to = 0, int mode = 0);
      void print_as_fasta();
      std::hash<std::string> mkhash;
      virtual const unsigned long get_seqhash();
      virtual const unsigned long get_idhash();

    protected:
      void changeCase(std::string& s, int mode = 0);
      Sequence(); // Force instantiation of sequences with name and sequence
      std::string seqid;
      std::string sequence;
      std::string seqtype;
      unsigned long seqhash;
      unsigned long idhash;
      unsigned seqlen;

  };
} // end namespace fasta
