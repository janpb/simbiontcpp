/*
 * dna.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Implementaion of the DNA class, which is derived form the Sequences class.
 * Version: 0
 */


#include <iostream>
#include "sequence/dna.h"


namespace sequence
{
  DNA::DNA(char* id, char* seq, std::string type)
  : Sequence(id, seq, type)
    { }

  DNA::DNA(std::string& id, std::string& seq, std::string type)
  : Sequence(id, seq, type)
    {
      //std::cerr << "Sequence::DNA: Ctor called for sequence: " << id << "\n";
    }

  DNA::~DNA()
    {
      //std::cerr << "Sequence::DNA: Destructor called for DNA " << seqid << "\n";
    }

  std::string DNA::protein(unsigned from, unsigned to, unsigned frame)
    { return translate.dna2prot(seq(), from, to, frame); }

  std::string DNA::mrna(unsigned from, unsigned to)
    { return translate.dna2mrna(seq(), from, to); }

  unsigned DNA::findStart(unsigned from, unsigned step)
  {
    for(unsigned i = from; i < length(); i += step)
    {
      std::string triplet = seq().substr(i, 3);
      if(translate.codon(triplet) == "M")
        { return i; }
    }
    return 0;
  }

  unsigned DNA::findStop(unsigned from, bool inFrame)
  {
    unsigned step = ((!inFrame) ? 1 : 3);
    for(unsigned i = from; i < length(); i += step)
    {
      std::string triplet = seq().substr(i, 3);
      if(translate.codon(triplet) == "*")
        { return i; }
    }
    return 0;
  }

  void DNA::find_init_positions()
  {
    for(unsigned i = 0; i < seqlen; ++i)
    {
      std::string triplet = sequence.substr(i, 3);
      if(translate.codon(triplet) == "M")
      {
        std::string orf = translate.get_orf(sequence.substr(i));
        init_pos[i] = orf.length();
      }
    }
  }

  void DNA::chomp()
  {
    std::string last_triplet = sequence.substr(sequence.size()-3);
    if(translate.codon(last_triplet) == "*")
    {
      sequence = sequence.substr(0, sequence.size()-3);
    }
  }

  std::map<int, int> DNA::get_init_positions()
  {
    find_init_positions();
    return init_pos;
  }
} // end namespace sequence
