/*
 * sequence/composition.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  The implementation file for mthods to analyze sequence compositions with different
 *  methods.
 * Version: 0
 */

#include <algorithm>
#include "composition.h"

namespace sequence
{
  Analyzer::Analyzer()
  {

  }

  Analyzer::~Analyzer()
  {

  }
  std::map<std::string, Nuc> Analyzer::nuc_analyzer(const std::string& seq)
  {
    std::string codon;
    for(unsigned i = 0; i < seq.length(); ++i)
    {
      std::string chr = seq.substr(i, 1);
      ++nuc_map[chr].occurrence;
      codon += chr;
      if(codon.length() == 3)
      {
        ++codons;
        codon.clear();
      }
    }
    for(auto& i : nuc_map)
    {
      i.second.freq = i.second.occurrence / seq.length();
    }
    return nuc_map;
  }

  void Analyzer::calcFreqAmac(const std::string& seq)
  {
    for(auto& i : seq)
    {
      ++amacs[i].occurrence;
      ++codons;
    }

    for(auto& i : amacs)
    {
      i.second.freq = i.second.occurrence / codons;
    }
  }

  void Analyzer::calcFreqCodon(const std::string& seq)
  {
    for(unsigned i = 3; i < seq.length(); i += 3)
    {
      std::string codon = seq.substr(i, 3);
      std::transform(codon.begin(), codon.end(), codon.begin(), ::toupper);
      if(gencode.count(codon) == 0)
      {
        std::cout << "Unknown codon: " << codon << " : Position: " << "("
                  << i << ")\n";
        break;
      }
      if(gencode[codon] == '*' )
      {
        continue;
      }

      for(unsigned k = 0; k < 3; ++k)
      {
        if(codon[k] == 'G' || codon[k] == 'C')
        {
          ++gc[0][k];
          ++gc[0][3];
        }
      }
      ++amacs[gencode[codon]].codons[codon][0];
      ++amacs[gencode[codon]].occurrence;
      ++codons; // The total number of codons analyzed
    }

    for(unsigned i = 0; i < 4; ++i)
    {
      gc[1][i] = gc[0][i] / (codons * 3);
    }

    for(auto& i : amacs)
    {
      for(auto& j : i.second.codons)
      {
        amacs[i.first].codons[j.first][1] =
          amacs[i.first].codons[j.first][0] / amacs[i.first].occurrence;
      }
      i.second.freq = i.second.occurrence / codons;
    }
  }

  std::map<char, Amac> Analyzer::getFreqAmac()
  {
    return amacs;
  }

  void Analyzer::print()
  {
    std::cout << "Codons total: " << codons << "\n";
    std::cout << "CG content:\tGC1\tGC2\tGC3\tGC\n\t";
    for(const auto& i : gc)
    {
      for(const auto& j : i){ std::cout << "\t" << j; }
      std::cout << "\n\t";
    }
    for(auto& i : amacs)
    {
      double   sum_frequency  = 0;
      unsigned sum_occurrence = 0;
      std::cout << "\nAmino acid: " << i.first                << "\n"
                << "Occurence: "    << i.second.occurrence    << "\n"
                << "Frequency: "    << i.second.freq          << "\n"
                                       "\tCodons | "          <<
                                       "occurrence (x_ij) | " <<
                                       "frequency (p_ij)\n";
      for(auto& j : i.second.codons)
      {
        std::cout << "\t" << j.first     << "\t\t" << j.second[0] << "\t\t" <<
                             j.second[1] << "\n";

        sum_occurrence += j.second[0];
        sum_frequency  += j.second[1];
      }
      std::cout << "\tTotal: \t\t" << sum_occurrence << "\t\t" <<
                                      sum_frequency  << "\n";
    }
  }


  Analyzer::Gencode Analyzer::gencode
  {
    {"ATG", 'M'}, // Methionine, Start, only one codon
    {"AGG", 'R'}, // Arginine
    {"AGA", 'R'},
    {"CGG", 'R'},
    {"CGA", 'R'},
    {"CGC", 'R'},
    {"CGT", 'R'},
    {"AGC", 'S'}, // Serine
    {"AGT", 'S'},
    {"TCG", 'S'},
    {"TCA", 'S'},
    {"TCC", 'S'},
    {"TCT", 'S'},
    {"TTG", 'L'}, // Leucine
    {"TTA", 'L'},
    {"CTG", 'L'},
    {"CTA", 'L'},
    {"CTC", 'L'},
    {"CTT", 'L'},
    {"ACG", 'T'}, // Threonine
    {"ACA", 'T'},
    {"ACC", 'T'},
    {"ACT", 'T'},
    {"GTG", 'V'}, // Valine
    {"GTA", 'V'},
    {"GTC", 'V'},
    {"GTT", 'V'},
    {"GGG", 'G'}, // Glycine
    {"GGA", 'G'},
    {"GGC", 'G'},
    {"GGT", 'G'},
    {"GCG", 'A'}, // Alanine
    {"GCA", 'A'},
    {"GCC", 'A'},
    {"GCT", 'A'},
    {"CCG", 'P'}, // Proline
    {"CCA", 'P'},
    {"CCC", 'P'},
    {"CCT", 'P'},
    {"ATA", 'I'}, // Isoleucine
    {"ATC", 'I'},
    {"ATT", 'I'},
    {"AAG", 'K'}, // Lysine
    {"AAA", 'K'},
    {"AAC", 'N'}, // Asparagine
    {"AAT", 'N'},
    {"TTC", 'F'}, // Phenylalanine
    {"TTT", 'F'},
    {"TAC", 'Y'}, // Tyrosine
    {"TAT", 'Y'},
    {"GAG", 'E'}, // Glutamatic acid
    {"GAA", 'E'},
    {"GAC", 'D'}, // Aspartatic acid
    {"GAT", 'D'},
    {"CAG", 'Q'}, // Glutamine
    {"CAA", 'Q'},
    {"CAC", 'H'}, // Histidine
    {"CAT", 'H'},
    {"TGC", 'C'}, // Cysteine
    {"TGT", 'C'},
    {"TGG", 'W'}, // Tryptophan, only one codon
    {"TGA", '*'}, // Stop codons
    {"TAG", '*'},
    {"TAA", '*'},
  };
} // end namespace sequence
