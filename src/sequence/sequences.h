#pragma once
/*
 * sequences.h
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */

#include "sequence/seq.h"
#include "sequence/dna.h"
//#include "sequence/amac.h"
