#pragma once
/*
 * dna.h
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Header for the DNA class. It is derived from the Sequence class.
 * Version: 0
 */

#include <iostream>
#include <algorithm>
#include "sequence/seq.h"
#include "sequence/translate.h"

namespace sequence
{
  class DNA: public Sequence
  {
    public:
      DNA(char* id, char* seq, std::string type = "dna");
      DNA(std::string& id, std::string& seq, std::string type = "dna");
      ~DNA();

      std::string protein(unsigned from  = 0,
                          unsigned to    = 0,
                          unsigned frame = 0);

      std::string mrna(unsigned from  = 0,
                       unsigned to    = 0);

      unsigned findStart(unsigned from = 0, unsigned step = 1);
      unsigned findStop(unsigned from = 0, bool inFrame = true);
      std::map<int, int> get_init_positions();
      void chomp();
      // implement reverse, reverse complement

    private:
      Translator translate;
      void find_init_positions();
      std::map<int, int> init_pos;
  };

}// end namespace sequence
