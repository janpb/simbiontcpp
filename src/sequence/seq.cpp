/*
 * fasta/seq.cpp
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  The implementations for the Sequence class. All positions on the sequence
 *  start at 0.
 *
 * Notes:
 * - Worried about the destructor and freeing memory.
 *
 * Version: 0
 */
#include "sequence/seq.h"

namespace sequence
{
  //  Constructors
  Sequence::Sequence()
  { }

  Sequence::Sequence(char* id, char* seq, std::string type)
  {
    this->seqid = std::string(id);
    this->sequence = std::string(seq);
    this->seqlen =  std::char_traits<char>::length(seq);
    this->seqtype = type;
    this->seqhash = mkhash(std::string(seq));
    this->idhash  = mkhash(std::string(id));
  }

  Sequence::Sequence(std::string& id, std::string& seq, std::string type)
  {
    //std::cerr << "Sequence::sequence: Ctor called for sequence: " << id << "\n";
    this->seqid = id;
    this->sequence = seq;
    this->seqlen =  seq.length();
    this->seqtype = type;
    this->seqhash = mkhash(seq);
    this->idhash = mkhash(id);
  }

  Sequence::~Sequence()
  {
    //std::cerr << "Sequence::sequence: Destructor called for sequence: " << seqid << "\n";
  }

  void Sequence::setId(std::string& id)
    { this->seqid = id; }

  void Sequence::setSeq(std::string& seq)
  {
    this->sequence = seq;
    this->seqlen = seq.length();
  }

  void Sequence::changeCase(std::string& s, int mode)
  {
    switch(mode)
    {
      case -1:
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
        break;
      case 1:
        std::transform(s.begin(), s.end(), s.begin(), ::toupper);
        break;
      /*case 2: make option for invert*/
      default:
        break;
    }
  }

  const std::string& Sequence::id()
    { return seqid; }

  const std::string& Sequence::seq()
    { return sequence; }

  const std::string& Sequence::type()
    { return seqtype; }

  const unsigned Sequence::length()
    { return seqlen; }

  const unsigned long Sequence::get_seqhash()
    {return seqhash; }

  const unsigned long Sequence::get_idhash()
    {return idhash; }

  std::string Sequence::subseq(unsigned from, unsigned to, int mode)
  {
    if(from == 0 && to == 0)
    {
      changeCase(sequence, mode);
      return  sequence;
    }

    if(to == 0)
    {
      std::string subs = sequence.substr(from);
      changeCase(subs, mode);
      return subs;
    }

    std::string subs = sequence.substr(from, (to - from));
    changeCase(subs, mode);
    return subs;
  }

  void Sequence::print_as_fasta()
  {
    std::cout << ">" << seqid << "\n"
              << sequence     << "\n";
  }
} //end namespace sequence
