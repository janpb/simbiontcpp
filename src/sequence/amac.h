#pragma once
/*
 * amac.h
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Protoypes for the amino acid classes. The idea is to use lookup table for
 *  the properties and not storing them for each amino acid.
 * Version: 0
 */
#include <vector>
#include <map>

namespace sequence
{
  class Chemistry
  {
    public:
      virtual char getSymbol() = 0;

    private:
      char symbol;
  };


  struct Aminoacid
  {
    std::string name;                 // 3-Letter code
    float hydro_idx;                  // hydropathy index
    unsigned mol_weigth;              // molecluar weight
    float pI;                         // isoelectric point
    float vdwv;                       // van der Waals volume
    std::vector<std::string> codons;  // the codons encoding the amino acid
  };

  class Amac: public Chemistry
  {
    public:
      Amac(const char code);
       ~Amac();
      char getSymbol();
      float getPI();
      float getVdwv();
      float getHydroIdx();
      unsigned getMolWeigth();
      std::string& getName();

    private:
      char symbol;
      std::string codon;
      typedef std::map<char, Aminoacid> Amac_tbl;
      static Amac_tbl amac_tbl;
  };



}// end namespace sequence
