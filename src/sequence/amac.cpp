/*
 * amac.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  The amino acid class. Implements amino acids with basic funcionality.
 * Version: 0
 */


#include <iostream>
#include "sequence/amac.h"

namespace sequence
{
  Amac::Amac(const char code)
  {
    // only one letter codes inplemented, yet
    symbol = code;
  }

   Amac::~Amac()
    { }

  std::string& Amac::getName()
    { return amac_tbl[symbol].name; }

  float Amac::getHydroIdx()
    { return amac_tbl[symbol].hydro_idx; }

  float Amac::getPI()
    { return amac_tbl[symbol].pI; }

  float Amac::getVdwv()
    { return amac_tbl[symbol].vdwv; }

  unsigned Amac::getMolWeigth()
    {return amac_tbl[symbol].mol_weigth; }

  char Amac::getSymbol()
    { return symbol; }



  Amac::Amac_tbl Amac::amac_tbl
  {
    /*Symbol, Abbrev.  hp_idx, mw[Da], pI[pH], van der Waals volume [A^3], codons          */
    {'A',   {"Ala",    1.8,    89,     6.01,   67,  {"GCT","GCC","GCA","GCG"            } }},
    {'C',   {"Cys",    2.5,   121,     5.02,   86,  {"TGT","TGC"                        } }},
    {'D',   {"Asp",   -3.5,   133,     2.85,   91,  {"GAT","GAC"                        } }},
    {'E',   {"Glu",   -3.5,   147,     3.15,  109,  {"GAA","GAG"                        } }},
    {'F',   {"Phe",    2.8,   165,     5.49,  135,  {"TTT","TTC"                        } }},
    {'G',   {"Gly",   -0.4,    75,     6.06,   48,  {"GGT","GGC","GGA","GGG"            } }},
    {'H',   {"His",   -3.2,   155,     7.60,  118,  {"CAT","CAC"                        } }},
    {'I',   {"Ile",    4.5,   131,     6.05,  124,  {"ATT","ATC","ATA"                  } }},
    {'K',   {"Lys",   -3.9,   146,     9.60,  135,  {"AAA","AAG"                        } }},
    {'L',   {"Leu",    3.8,   131,     6.01,  124,  {"TTA","TTG","CTT","CTC","CTA","CTG"} }},
    {'M',   {"Met",    1.9,   149,     5.74,  124,  {"ATG"                              } }},
    {'N',   {"Asn",   -3.5,   132,     5.41,   96,  {"AAT","AAC"                        } }},
    {'P',   {"Pro",   -1.6,   115,     6.30,   90,  {"CCT","CCC","CCA","CCG"            } }},
    {'Q',   {"Gln",   -3.5,   146,     5.65,  114,  {"CAA","CAG"                        } }},
    {'R',   {"Arg",   -4.5,   174,    10.76,  148,  {"CGT","CGC","CGA","CGG"            } }},
    {'S',   {"Ser",   -0.8,   105,     5.68,   73,  {"TCT","TCC","TCA","TCG","AGT","AGC"} }},
    {'T',   {"Thr",   -0.7,   119,     5.60,   93,  {"ACT","ACC","ACA","ACG"            } }},
    {'V',   {"Val",    4.2,   117,     6.00,  105,  {"GTT","GTC","GTA","GTG"            } }},
    {'W',   {"Trp",   -0.9,   204,     5.89,  163,  {"TGG"                              } }},
    {'Y',   {"Tyr",   -1.3,   181,     5.64,  141,  {"TAT","TAC"                        } }},
    {'*',   {"STP",    0.0,     0,        0,    0,  {"TAA","TAG","TGA"                  } }},
    /*  Symbols without chemical properties but used in alignments                        */
    {'-',   {"GAP",    0.0,     0,        0,    0,  {                                   } }},
    {'N',   {"NAA",    0.0,     0,        0,    0,  {                                   } }},
    {'X',   {"XAA",    0.0,     0,        0,    0,  {                                   } }}
  };
}//end namesapce sequence
