/*
 * translate.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */

#include <map>
#include <iostream>
#include <algorithm>
#include "sequence/translate.h"

namespace sequence
{
  Translator::Translator()
    { }

  Translator::~Translator()
    { }

  std::string Translator::dna2prot(const std::string& seq,
                                   unsigned beg,
                                   unsigned end,
                                   unsigned frame)
  {
    end = setEnd(end, seq.length());
    std::string protein;
    for(unsigned i = beg + frame; i < end; i += 3)
    {
      std::string triplet = seq.substr(i, 3);
      protein += codon(triplet);
    }
    return protein;
  }

  std::string Translator::dna2mrna(const std::string& seq,
                                   unsigned beg,
                                   unsigned end)
  {
    end = setEnd(end, seq.length());
    std::string mrna = "";
    for(unsigned i = beg; i < end; ++i)
    {
      if(seq[i] == 'T')
        { mrna += "U"; }
      else if(seq[i] == 't')
        { mrna += "u"; }
      else
        { mrna += seq[i]; }
    }
    return mrna;
  }

  std::string Translator::mrna2dna(const std::string& seq,
                                   unsigned beg,
                                   unsigned end)
  {
    end = setEnd(end, seq.length());
    std::string dna = "";
    for(unsigned i = beg; i < end; ++i)
    {
      if(seq[i] == 'U')
        { dna += "T"; }
      else if(seq[i] == 'u')
        { dna += "T"; }
      else
        { dna += seq[i]; }
    }
    return dna;
  }

  unsigned Translator::setEnd(unsigned end, unsigned max)
    { return ((end == 0 || end > max ) ? max : end); }

  std::string Translator::get_orf(const std::string& seq)
  {
    std::string orf;
    for(unsigned i = 0; i < seq.length(); i += 3)
    {
      std::string triplet = seq.substr(i, 3);
      if(codons[triplet] == "*")
      {
        return seq.substr(0, i+3);
      }
    }
    return seq;
  }

  std::string Translator::codon(std::string& triplet)
  {
    std::transform(triplet.begin(), triplet.end(),
                   triplet.begin(),
                   ::toupper);

    if(codons.count(triplet) != 0)
      { return codons[triplet]; }
    return "X";
  }

  Translator::Codons Translator::codons
  {
    /*          T             C             A             G          */
    /*T*/ {"TTT", "F"}, {"TCT", "S"}, {"TAT", "Y"}, {"TGT", "C"}, /*T*/
          {"TTC", "F"}, {"TCC", "S"}, {"TAC", "Y"}, {"TGC", "C"}, /*C*/
          {"TTA", "L"}, {"TCA", "S"}, {"TAA", "*"}, {"TGA", "*"}, /*A*/
          {"TTG", "L"}, {"TCG", "S"}, {"TAG", "*"}, {"TGG", "W"}, /*G*/
    /*C*/ {"CTT", "L"}, {"CCT", "P"}, {"CAT", "H"}, {"CGT", "R"}, /*T*/
          {"CTC", "L"}, {"CCC", "P"}, {"CAC", "H"}, {"CGC", "R"}, /*C*/
          {"CTA", "L"}, {"CCA", "P"}, {"CAA", "Q"}, {"CGA", "R"}, /*A*/
          {"CTG", "L"}, {"CCG", "P"}, {"CAG", "Q"}, {"CGG", "R"}, /*G*/
    /*A*/ {"ATT", "I"}, {"ACT", "T"}, {"AAT", "N"}, {"AGT", "S"}, /*T*/
          {"ATC", "I"}, {"ACC", "T"}, {"AAC", "N"}, {"AGC", "S"}, /*C*/
          {"ATA", "I"}, {"ACA", "T"}, {"AAA", "K"}, {"AGA", "R"}, /*A*/
          {"ATG", "M"}, {"ACG", "T"}, {"AAG", "K"}, {"AGG", "R"}, /*G*/
    /*G*/ {"GTT", "V"}, {"GCT", "A"}, {"GAT", "D"}, {"GGT", "G"}, /*T*/
          {"GTC", "V"}, {"GCC", "A"}, {"GAC", "D"}, {"GGC", "G"}, /*C*/
          {"GTA", "V"}, {"GCA", "A"}, {"GAA", "E"}, {"GGA", "G"}, /*A*/
          {"GTG", "V"}, {"GCG", "A"}, {"GAG", "E"}, {"GGG", "G"}, /*G*/
  };
} // end namespace sequence
