#pragma once
/*
 * translate.h
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 */


#include <iostream>
namespace sequence
{
  class Translator
  {
    public:
    Translator();
    ~Translator();

      std::string dna2prot(const std::string& seq,
                           unsigned beg = 0,
                           unsigned end = 0,
                           unsigned frame = 0);

      std::string dna2mrna(const std::string& seq,
                           unsigned beg = 0,
                           unsigned end = 0);

      std::string mrna2dna(const std::string& seq,
                           unsigned beg = 0,
                           unsigned end = 0);

      std::string codon(std::string& triplet);

      std::string get_orf(const std::string& seq);

    private:
      unsigned setEnd(unsigned end, unsigned max);
      typedef std::map<std::string, std::string> Codons;
      static Codons codons;


  };
}//end namespace sequence
