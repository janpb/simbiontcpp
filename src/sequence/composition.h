#pragma once
/*
 * sequence/composition.h
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  The header file for mthods to analyze sequence compositions with different
 *  methods.
 * Version: 0
 */

#include <iostream>
#include <vector>
#include <string>
#include <map>

namespace sequence
{
  struct Amac
  {
    // codon = {occurrence, frequency, entropy}
    std::map<std::string, double[3]> codons;
    double freq;
    double occurrence;
  };

  struct Nuc
  {
    double occurrence;
    double freq;
  };

  class Analyzer
  {
    public:
      Analyzer();
      ~Analyzer();
      void calcFreqCodon(const std::string&);
      void calcFreqAmac(const std::string&);
      std::map<std::string, Nuc> nuc_analyzer(const std::string&);
      std::map<char, Amac> getFreqAmac();
      //void calcShannonEntropy();

    private:
      // GC frequency
      // occurence: [0][GC1][GC2][GC3][GC]
      // frequency: [1][GC1][GC2][GC3][GC]
      double gc[2][4];
      std::map<std::string, Nuc> nuc_map;
      std::map<char, Amac> amacs;
      typedef std::map<std::string, char> Gencode;
      static Gencode gencode;
      double codons;
      void print();

  };
} // end namespace sequence
