/*
 * fasta.filter.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *
 * Version: 0
 *
 * ToDo: Think about using the sequence composition
 */


#include <iostream>
#include <boost/program_options.hpp>
//#include <boost/regex.hpp>
#include <fstream>
#include <string>
#include "fasta/read.h"
#include "sequence/composition.h"

class Filter
{
  public:
    Filter();
    virtual ~Filter();
    virtual bool filter(sequence::Sequence*) = 0;

  private:

};

Filter::Filter()
  {}
Filter::~Filter()
  {}

class CompositionFilter: public Filter
{
  public:
    CompositionFilter(const std::vector<std::string>&);
    ~CompositionFilter();
    bool filter(sequence::Sequence* s);
  private:
    void parse_values(const std::vector<std::string>&);
    std::map<std::string, unsigned[2]> filter_map;

};

CompositionFilter::CompositionFilter(const std::vector<std::string>& vals)
{
  parse_values(vals);
}

CompositionFilter::~CompositionFilter()
{

}

bool CompositionFilter::filter(sequence::Sequence* s)
{
  sequence::Analyzer seqa;
  for(auto& i : seqa.nuc_analyzer(s->subseq(0,0,1)) )
  {
    if(filter_map.count(i.first) > 0)
    {
      //std::cout << i.first << " " << i.second.occurrence << "\n";
      //std::cout << "Min: " << filter_map[i.first][0] << "\n"
                //<< "Max: " << filter_map[i.first][1] << "\n";
      //std::cout << (filter_map[i.first][0] > 0) << "\n";
      //std::cout << (filter_map[i.first][1] > 0) << "\n";
      //std::cout << "----\n";

      if( (filter_map[i.first][0] > 0) & (filter_map[i.first][1] > 0) )
      {
        if( (i.second.occurrence >= filter_map[i.first][0]) &
            (i.second.occurrence >= filter_map[i.first][1]) )
        {
          return true;
        }
        return false;
      }

      if( (filter_map[i.first][0] > 0) &
          (i.second.occurrence > filter_map[i.first][0]) )
      {
        return true;
      }

      if( (filter_map[i.first][1] > 0) &
          (i.second.occurrence > filter_map[i.first][1]) )
      {
        return true;
      }
    }
  }
  return false;
}

void CompositionFilter::parse_values(const std::vector<std::string>& vals)
{
  for(auto& i : vals)
  {
    unsigned max_occurence = 1;
    unsigned min_occurence = 1;
    int pos_delim = i.find(':');
    std::string chr = i.substr(0, pos_delim);
    if(i.substr(pos_delim+1).size()  > 0)
    {
      int pos_subdelim = i.substr(pos_delim+1).find(':');
      if(pos_subdelim < 0)
      {
        pos_subdelim = 0;
        max_occurence = 0;
        min_occurence = std::stoul(i.substr(pos_delim+1));
      }
      else
      {
        min_occurence = i.substr(pos_delim+pos_subdelim+2).size() > 0 ?
                        std::stoul(i.substr(pos_delim+1, pos_subdelim)) : 1;

        max_occurence = i.substr(pos_delim+pos_subdelim+2).size() > 0 ?
                        std::stoul(i.substr(pos_delim+pos_subdelim+2)) : 0;
      }
    }
    filter_map[chr][0] = min_occurence;
    filter_map[chr][1] = max_occurence;
  }
}

class FilterLen: public Filter
{
  public:
    FilterLen(const std::string& s);
    ~FilterLen();
    bool filter(sequence::Sequence*);

  private:
    void split_pos(const std::string& s);
    void swap(unsigned& i, unsigned& j);
    unsigned min = 0;
    unsigned max = 0;
};

  FilterLen::FilterLen(const std::string& s)
  {
    split_pos(s);
  }
  FilterLen::~FilterLen()
    { }

  bool FilterLen::filter(sequence::Sequence* s)
  {
    unsigned loc_max = max;
    if(max == 0){ loc_max = s->length(); }
    if( (s->length() >= min) & (s->length() <= loc_max) ){ return false; }
    return true;
  }

  void FilterLen::swap(unsigned& i, unsigned& j)
  {
    int tmp = i;
    i = j;
    j = tmp;
  }

  void FilterLen::split_pos(const std::string& s)
  {
    int pos = s.find(':');
    if(pos == -1)
    {
      min = std::stoul(s);
      max = 0;
    }
    else
    {
      min = s.substr(0, pos).size() > 0 ? std::stoul(s.substr(0, pos)) : 0;
      max = s.substr(pos+1).size()  > 0 ? std::stoul(s.substr(pos+1))  : 0;
      if( (min > 0) & (max > 0) )
      {
        if( (max < min) | (min > max) ){ swap(min, max); }
      }
    }
  }

class IupacFilter: public Filter
{
  public:
    IupacFilter(bool);
    ~IupacFilter();
    bool filter(sequence::Sequence* s);

  private:
    //boost::regex non_iupac;
    //boost::smatch m;
    std::map<std::string, int> non_iupac
    {
      {"B", 1},
      {"J", 1},
      {"O", 1},
      {"Z", 1}
    };
};

IupacFilter::IupacFilter(bool)
  {}

IupacFilter::~IupacFilter()
  {}

bool IupacFilter::filter(sequence::Sequence* s)
{
  sequence::Analyzer seqa;
  for(auto& i : seqa.nuc_analyzer(s->subseq(0,0,1)) )
  {
    if(non_iupac.count(i.first) > 0){ return true; }
  }
  return false;
  //non_iupac = "[BJOZ]+";
  //if(boost::regex_match(s->subseq(0,0,1), m, non_iupac))
  //{
    //return true;
  //}
  //return false;
}

class AmbFilter: public Filter
{
  public:
    AmbFilter(const std::string&);
    ~AmbFilter();
    bool filter(sequence::Sequence* s);

  private:
    std::string mtype;
    std::map<std::string, int> nonambigous_nt
    {
      {"A", 1},
      {"C", 1},
      {"T", 1},
      {"G", 1},
      {"U", 1},
    };

    std::map<std::string, int> ambigous_na
    {
      {"B", 1},
      {"Z", 1},
      {"X", 1},
   //   {"N", 1}
    };
};

AmbFilter::AmbFilter(const std::string& mtype)
{
  this->mtype = mtype;
}

AmbFilter::~AmbFilter()
  {}

bool AmbFilter::filter(sequence::Sequence* s)
{
  sequence::Analyzer seqa;
  if(mtype == "nt")
  {
    for(auto& i : seqa.nuc_analyzer(s->subseq(0,0,1)) )
    {
      if(nonambigous_nt.count(i.first) == 0)
      {
        return true;
      }
    }
    return false;
  }
  else if(mtype == "na")
  {
    for(auto& i : seqa.nuc_analyzer(s->subseq(0,0,1)) )
    {
      if(ambigous_na.count(i.first) > 0)
      {
        return true;
      }
    }
    return false;
  }
  else
  {
    std::cerr << "Unknown molecular type: " << mtype << ".\n";
    return false;
  }
}

class FileFilter: public Filter
{
  public:
    FileFilter(std::string, bool keep);
    ~FileFilter();
    bool filter(sequence::Sequence* s);

  private:
    std::map<std::string, int> seqmap;
    void prep_list(std::string&);
    bool keep;
};

FileFilter::FileFilter(std::string fname, bool keep)
:keep(keep)
{
  prep_list(fname);

}
FileFilter::~FileFilter()
  {}

void FileFilter::prep_list(std::string& fname)
{
  std::ifstream file(fname);
  std::string header;
  while(std::getline(file, header))
  {
    if(header[0] == '>')
    {
      header.erase(0);
    }
    seqmap[header] = 1;
  }
}

bool FileFilter::filter(sequence::Sequence* s)
{
  if(keep == true)
  {
    if(seqmap.count(s->id()) > 0)
    {
      return false;
    }
    return true;
  }
  else
  {
    if(seqmap.count(s->id()) > 0)
    {
      return true;
    }
    return false;
  }
}

class Filtration:public fasta::Reader::Provider
{
  public:
    Filtration(boost::program_options::variables_map& vm);
    ~Filtration();
    void provide(sequence::Sequence*);

  private:
    void add_filter(boost::program_options::variables_map& vm);
    void set_moltype(const std::string&);
    std::vector<Filter*> fveq;
    std::string moltype = "nt";
};


void Filtration::set_moltype(const std::string& mtype)
{
  moltype = mtype;
}

Filtration::Filtration(boost::program_options::variables_map& vm)
{
  if(vm.count("moltype"))
  {
    set_moltype(vm["moltype"].as<std::string>());
  }
  add_filter(vm);
}

Filtration::~Filtration()
{
  for(auto& i : fveq)
  {
    delete i;
  }
}

void Filtration::add_filter(boost::program_options::variables_map& vm)
{
  if(vm.count("length"))
  {
    FilterLen* f = new FilterLen(vm["length"].as<std::string>());
    fveq.push_back(f);
  }
  if(vm.count("char"))
  {
    CompositionFilter* cf = new CompositionFilter(vm["char"].as< std::vector<std::string> >());
    fveq.push_back(cf);
  }

  if(vm.count("iupac") and vm["iupac"].as<bool>() == true)
  {
    IupacFilter* iupacf = new IupacFilter(vm["iupac"].as<bool>());
    fveq.push_back(iupacf);
  }

  if(vm.count("ambigous") and vm["ambigous"].as<bool>() == true)
  {
    AmbFilter* ambf = new AmbFilter(moltype);
    fveq.push_back(ambf);
  }

  if(vm.count("file"))
  {
    FileFilter* filef = new FileFilter(vm["file"].as<std::string>(), false);
    fveq.push_back(filef);
  }

  if(vm.count("File"))
  {
    FileFilter* fileF = new FileFilter(vm["File"].as<std::string>(), true);
    fveq.push_back(fileF);
  }

}

void Filtration::provide(sequence::Sequence* seq)
{
  bool reject = false;
  for(auto &i : fveq)
  {
    if(i->filter(seq) == true)
    {
      reject = true;
      break;
    }
  }
  if(reject == false)
  {
    std::cout << ">" << seq->id()  << "\n"
                     << seq->seq() << "\n";
  }
  //std::cout << "------------------------------------\n";
}


int main(int argc, char **argv)
{
  boost::program_options::options_description desc("Filter options:");
  desc.add_options()
    ("help,h", "print usage")

    ("moltype,m", boost::program_options::value<std::string>(),
     "Set molecular type of sequences: nt(nucleotide, default) | na (amino acids)")

    ("length,l", boost::program_options::value<std::string>(),
     "Filter by length\n\tmin_len:max_len\n\tExample: -l 10:100")

    ("char,c",   boost::program_options::value<std::vector<std::string>>()->multitoken()->
                 zero_tokens()->composing(),
     "Filter by char <char:min_occurrence:max_occurrence>")

    ("iupac,i",    boost::program_options::bool_switch()->default_value(true),
     "Filter sequences containing only IUPAC codes")

    ("ambigous,a", boost::program_options::bool_switch()->default_value(false),
     "Filter sequences containing ambigous codes (B, Z, N, X)")

    ("file,f", boost::program_options::value<std::string>(),
     "Remove sequences listed file from input")

    ("File,F", boost::program_options::value<std::string>(),
     "Remove sequences not listed in file from input. Negated -f")
    ;
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);
  if(vm.count("help"))
  {
    std::cout << desc << "\n";
    return 1;
  }


  fasta::Reader reader(std::cin);
  Filtration f(vm);
  reader.iterate(f);
  return 0;
}
