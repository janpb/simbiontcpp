#pragma once
/*
 * scuo.h
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Header filefor the SCUO implementation
 *
 * Version: 0
 */

#include <iostream>
#include <vector>
#include <string>
#include <map>

namespace scuo
{
  static const double HMIN = 0;

  struct Amac
  {
    // codon = {occurrence, frequency, entropy}
    std::map<std::string, double[3]> codons;
    double entropy_aa;
    double entropy_codons;
    double entropy_max;
    double freq;
    double scuo;
    unsigned occurrence;
  };

  class SCUO
  {
    public:
      SCUO();
      ~SCUO();
      void calcCodonFreq(std::string&);
      void calcShannonEntropy();

    private:
      typedef std::map<std::string, char> Gencode;
      static Gencode gencode;
      void printAmac();
      std::map<char, Amac> amacs;
      void initAmacs();
      double amac_total;
  };
} // end namespace scuo
