/*
 * scuo.cpp
 *
 * Copyright 2015 The University of Sydney
 * Author: Jan P Buchmann <lejosh@members.fsf.org>
 * Description:
 *  Implementation of SCUO
 * Version: 0
 */


#include <iostream>
#include <algorithm>
#include <cmath>
#include "scuo/scuo.h"

namespace scuo
{
  SCUO::SCUO()
  {
    initAmacs();
  }
  SCUO::~SCUO()
  {

  }

  void SCUO::calcCodonFreq(std::string& seq)
  {
    // Have the suspicion that the second loop can be incorporated into the
    // first on. However, always missing the last data point
    amac_total = 0;
    for(unsigned i = 0; i < seq.length(); i += 3)
    {
      std::string codon = seq.substr(i, 3);
      std::transform(codon.begin(), codon.end(), codon.begin(), ::toupper);
      if(gencode.count(codon) == 0){ continue; }
      ++amac_total;
      ++amacs[gencode[codon]].codons[codon][0];
      ++amacs[gencode[codon]].occurrence;
    }

    for(auto& i : amacs)
    {
      for(auto& j : i.second.codons)
      {
        amacs[i.first].codons[j.first][1] =
          amacs[i.first].codons[j.first][0] / amacs[i.first].occurrence;
      }
      i.second.freq = i.second.occurrence / amac_total;
    }
    //printAmac();
  }

  void SCUO::calcShannonEntropy()
  {
    double avrg_scuo = 0;
    for(auto& i : amacs)  // i = amino acids
    {
      double ratio = 0;
      /*************************************************************************
       * This block should calculate the Shannon entropy for each amino acid i
       * by calulating the Shannon entropy for each codon coding for amino
       * acid i, e.g. the six codons encoding Leucin should add up to the
       * total Entropy for Leucin. However, while the frequency for each codon
       * adds up to the frequency for the amino acid, Shannon does not. Why?*/

      for(auto& j : i.second.codons) // j = codons
      {
        if(j.second[1] == 0 || j.second[1] == 1)
        {
          j.second[2] = HMIN;
          //i.second.entropy = HMIN;
          continue;
        }
        j.second[2] = -j.second[1] * log(j.second[1]);
        i.second.entropy_codons += j.second[2];
      }
      /************************************************************************/
      // scuo == 0, random codon usage
      // scuo == 1, biased codon usage
      i.second.entropy_aa = -i.second.freq * log(i.second.freq);
      i.second.scuo = (i.second.entropy_max - i.second.entropy_codons) /
                              i.second.entropy_max;
      for(auto& k : amacs)
      {
        ratio += k.second.occurrence;
      }
      ratio = i.second.occurrence/ratio;
      avrg_scuo += ratio * i.second.scuo;
    }
    std::cout << avrg_scuo << "\n";
    printAmac();
  }

  void SCUO::initAmacs()
  {
    for(auto& i : gencode)
    {
      amacs[i.second].codons[i.first][0] = 0;
      amacs[i.second].codons[i.first][1] = 0;
      // casting int to double to avoid new variable. Good or bad?
      if( amacs[i.second].codons.size() == 1){amacs[i.second].entropy_max = 0;}
      else
      {
        amacs[i.second].entropy_max =
          -log( 1 / static_cast<double>(amacs[i.second].codons.size()) );
      }
    }
    //printAmac();
  }

  void SCUO::printAmac()
  {
    std::cout << "\nSummary:\nCodons total: " << amac_total << "\n";
    for(auto& i : amacs)
    {
      unsigned sum_occurrence = 0;
    double sum_frequency  = 0;
    double sum_entropy    = 0;
      std::cout << "\nAmino acid: " << i.first                << "\n"
                << "Occurence: "    << i.second.occurrence    << "\n"
                << "Frequency: "    << i.second.freq          << "\n"
                << "Hmax_i = "      << i.second.entropy_max   << "\n"
                << "H_i    = "      << i.second.entropy_aa    << "\n"
                << "SCUO   = "      << i.second.scuo          << "\n"
                                       "\tCodons | "          <<
                                       "occurrence (x_ij) | " <<
                                       "frequency (p_ij)  | " <<
                                       "entropy (H_ij)\n";
      for(auto& j : i.second.codons)
      {
        std::cout << "\t" << j.first     << "\t\t" <<
                             j.second[0] << "\t\t" <<
                             j.second[1] << "\t "  <<
                             j.second[2] << "\n";

        sum_occurrence += j.second[0];
        sum_frequency  += j.second[1];
        sum_entropy    += j.second[2];
      }
      std::cout << "\tTotal: \t\t" <<
                    sum_occurrence << "\t\t" <<
                    sum_frequency  << "\t "  <<
                    sum_entropy    << "\n";
    }
  }


  SCUO::Gencode SCUO::gencode
  {
    //{"ATG", 'M'}, // Methionine, Start, only one codon
    {"AGG", 'R'}, // Arginine
    {"AGA", 'R'},
    {"CGG", 'R'},
    {"CGA", 'R'},
    {"CGC", 'R'},
    {"CGT", 'R'},
    {"AGC", 'S'}, // Serine
    {"AGT", 'S'},
    {"TCG", 'S'},
    {"TCA", 'S'},
    {"TCC", 'S'},
    {"TCT", 'S'},
    {"TTG", 'L'}, // Leucine
    {"TTA", 'L'},
    {"CTG", 'L'},
    {"CTA", 'L'},
    {"CTC", 'L'},
    {"CTT", 'L'},
    {"ACG", 'T'}, // Threonine
    {"ACA", 'T'},
    {"ACC", 'T'},
    {"ACT", 'T'},
    {"GTG", 'V'}, // Valine
    {"GTA", 'V'},
    {"GTC", 'V'},
    {"GTT", 'V'},
    {"GGG", 'G'}, // Glycine
    {"GGA", 'G'},
    {"GGC", 'G'},
    {"GGT", 'G'},
    {"GCG", 'A'}, // Alanine
    {"GCA", 'A'},
    {"GCC", 'A'},
    {"GCT", 'A'},
    {"CCG", 'P'}, // Proline
    {"CCA", 'P'},
    {"CCC", 'P'},
    {"CCT", 'P'},
    {"ATA", 'I'}, // Isoleucine
    {"ATC", 'I'},
    {"ATT", 'I'},
    {"AAG", 'K'}, // Lysine
    {"AAA", 'K'},
    {"AAC", 'N'}, // Asparagine
    {"AAT", 'N'},
    {"TTC", 'F'}, // Phenylalanine
    {"TTT", 'F'},
    {"TAC", 'Y'}, // Tyrosine
    {"TAT", 'Y'},
    {"GAG", 'E'}, // Glutamatic acid
    {"GAA", 'E'},
    {"GAC", 'D'}, // Aspartatic acid
    {"GAT", 'D'},
    {"CAG", 'Q'}, // Glutamine
    {"CAA", 'Q'},
    {"CAC", 'H'}, // Histidine
    {"CAT", 'H'},
    {"TGC", 'C'}, // Cysteine
    {"TGT", 'C'},
    //{"TGG", 'W'}, // Tryptophan, only one codon
  };
} // end namespace scuo
