/*
* fasta.split.cpp
* Copyright 2015 The University of Sydney
* Author: Jan P Buchmann <lejosh@members.fsf.org>
*
* Description:
*  Splitting FASTA flat files into sepearte FASTA files.
*
* version: 1
*/
#include <iostream>
#include "fasta/read.h"

class Info:public fasta::Reader::Provider
{
  public:
    void provide(sequence::Sequence*);
    void summarize();

  private:
    unsigned tot_seq = 0;
    unsigned tot_len = 0;
    unsigned min = 0;
    unsigned max = 0;
  //  const std::string setFilename(std::string);
};

void Info::provide(sequence::Sequence* seq)
{
  ++tot_seq;
  tot_len += seq->length();
  if(tot_seq == 1)
  {
    min = seq->length();
    max = seq->length();
  }
  std::cout << seq->id() << "\t" << seq->length() << "\n";
  min = (min < seq->length()) ? min : seq->length();
  max = (max > seq->length()) ? max : seq->length();
}

void Info::summarize()
{

  std::cout << "#Sequences\t\ttotal\tmin\tmax\tmean\n##"
            << tot_seq                << "\t"
            << tot_len                << "\t"
            << min                    << "\t"
            << max                    << "\t"
            << tot_len / tot_seq      << "\n";
}


int main(int argc, char **argv)
{
  fasta::Reader reader(std::cin);
  Info i;
  reader.iterate(i);
  i.summarize();
  return 0;
}
